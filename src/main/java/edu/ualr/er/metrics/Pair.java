package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.Pair
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Pair-wise information for two references
 * 
 * Left and right link of the pair are arbitrary and depend on
 * how the pairs are evaluated from the link sets
 * 
 * @author James True
 *
 */
public class Pair {

	//private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * Left reference of the pair
	 */
	protected Link leftLink;
	public Link getLeftLink() {
		return this.leftLink;
	}
	public Pair setLeftLink(Link link) {
		if (link == null) {
			throw new IllegalArgumentException("Mising Left Link");
		}
		this.leftLink = link;
		return this;
	}
	
	/**
	 * Right reference of the pair
	 */
	protected Link rightLink;
	public Link getRightLink() {
		return this.rightLink;
	}
	public Pair setRightLink(Link link) {
		if (link == null) {
			throw new IllegalArgumentException("Mising Right Link");
		}
		this.rightLink = link;
		return this;
	}

	/**
	 * No-argument constructor
	 */
	protected Pair() {
		leftLink = null;
		rightLink = null;
	}
	
	/**
	 * Constructor taking two references
	 *	LinkID and TruthID are taken from ref1 
	 *
	 * @param ref1
	 * @param ref2
	 */
	public Pair (Link leftLink, Link rightLink) {
		if (leftLink == null) {
			throw new IllegalArgumentException("Mising Left Link");
		}
		this.leftLink = leftLink;
		if (rightLink == null) {
			throw new IllegalArgumentException("Mising Right Link");
		}
		this.rightLink = rightLink;
	}

	/**
	 * Override toString
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (leftLink != null) {
			sb.append("Left[").append(this.leftLink.toString()).append("] ");
		}
		if (rightLink != null) {
			sb.append("Right[").append(this.rightLink.toString()).append("] ");
		}
		return sb.toString();
	}
}
