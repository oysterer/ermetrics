package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.EncryptedTruth
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.ZipInputStream;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

/**
 * 
 * Abstract base class for encrypted truth sets
 * All of the logic is in this class except the method that provides the encryption token
 * 
 * Currently the only supported subclass is EncryptedTruthImpl
 * That is not include here because this is an open source project
 * To create and use encrypted truth sets, create the subclass
 * with the getToken() method to return your specific encryption string.
 * 
 * @author James True
 *
 */

public abstract class EncryptedTruth {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * The name of this zip file
	 */
	private String zipName;
	public String getFileName() {
		return zipName;
	}
	public EncryptedTruth setFileName(String zipName) {
		this.zipName = zipName;
		return this;
	}
	
	/**
	 * Constructor 
	 */
	public EncryptedTruth() {
		this.zipName = null;
	}
	
	/*
	 * If getToken is overridden in a subclass
	 * and returns a key then this will be true
	 */
	public boolean isEnabled() {
		return (getToken() != null);
	}
	
	/*
	 * Simple test based on the file extension
	 * Could also actually try to pen the file and validate that it is a zip file
	 */
	public boolean isEncrypted() {
		return isFileEncrypted(this.zipName);
	}
	
	/*
	 * Simple test based on the file extension
	 * Could also actually try to pen the file and validate that it is a zip file
	 */
	public static boolean isFileEncrypted(String fileName) {
		String ext = FilenameUtils.getExtension(fileName);
		return StringUtils.equalsIgnoreCase(ext, "ERT");
	}

	/**
	 * Copy the specified reference file to the zip
	 * @param zipName
	 * @return
	 */
	public boolean encrypt(String... fileNames) {

		// NPE protection
		if (fileNames == null) {
			log.error("No files to process");
			return false;
		}
		log.info("Creating encrypted reference: {}", zipName);
		
		// Allocate the array
		ArrayList<File> files = new ArrayList<File>(fileNames.length);

		for (String fileName : fileNames) {
			if (StringUtils.isBlank(fileName)) {
				log.error("No filename specified");
				return false;
			}
			File file = new File(fileName);
			// If the file does not exist, error out
			if (!file.exists()) {
				log.error("File {} not found", fileName);
				return false;
			}
			// add it to the list
			log.info("Including {}", fileName); 
			files.add(file);
		}
		
		File zipFile = new File(zipName);
		// If the file exists, delete it
		if (zipFile.exists()) {
			log.info("Removing existing file {}", zipName);
			zipFile.delete();
		}

		boolean result = true;
		try {
			// This is name and path of zip file to be created
			ZipFile zip = new ZipFile(zipName);
			// Initiate EncryptedTruth Parameters which define various properties
			ZipParameters parameters = new ZipParameters();
			// set compression method to deflate compression
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);

			// DEFLATE_LEVEL_FASTEST - Lowest compression level but higher speed of
			// compression
			// DEFLATE_LEVEL_FAST - Low compression level but higher speed of compression
			// DEFLATE_LEVEL_NORMAL - Optimal balance between compression level/speed
			// DEFLATE_LEVEL_MAXIMUM - High compression level with a compromise of speed
			// DEFLATE_LEVEL_ULTRA - Highest compression level but low speed
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);

			// Set the encryption flag to true
			parameters.setEncryptFiles(true);

			// Set the encryption method to AES EncryptedTruth Encryption
			parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);

			// AES_STRENGTH_128 - For both encryption and decryption
			// AES_STRENGTH_192 - For decryption only
			// AES_STRENGTH_256 - For both encryption and decryption
			// Key strength 192 cannot be used for encryption. But if a zip file already has
			// a file encrypted with key strength of 192, then Zip4j can decrypt this file
			parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);

			// Set password
			parameters.setPassword(getToken());
			
			// Now add the files to the zip
			zip.addFiles(files, parameters);

		} catch (ZipException e) {
			result = false;
			log.error("Error zipping truth file {}", e.getMessage(), e);
		}
		return result;
	}
	
	// Get the stored parameters from the zip and set the reference options
	// TODO read the properties file and set the reference header values
	public boolean getProperties(Configuration config) {
		config.getReferenceDelimiter();
		config.getReferenceRecordColumns();
		config.getReferenceClusterColumn(); 
		return true;
	}

	public InputStream getReferenceStream(Configuration config) {
		
		File file = new File(zipName);
		
		// validate the zip file exists
		if (!file.exists()) {
			log.error("File {} not found", zipName);
			return null;
		}
		
		ZipInputStream is = null;
		try {
			// This is name and path of zip file to be created
			ZipFile zip = new ZipFile(zipName);

			// Check to see if the zip file is password protected
			if (zip.isEncrypted()) {
				zip.setPassword(getToken());
			}
			
			// Process all the files
			@SuppressWarnings("unchecked")
			List<FileHeader> headers = zip.getFileHeaders();
			if (headers.size() < 2) {
				log.error("File {} contents invalid", zipName);
				return null;
			}
			for (FileHeader header : headers) {
				if (StringUtils.endsWith(header.getFileName(),".properties")) {
					properties(zip.getInputStream(header),config);
				} else {
					is = zip.getInputStream(header);				}
			}
		} catch (ZipException e) {
			log.error("EncryptedTruth error:", e.getMessage());
			return null;
		}
		return is;
	}
	
	protected void properties(InputStream stream, Configuration config) {
		Configuration props = new Configuration();
		props.load(stream);
	}
	
	protected String getToken() {
		return null;
	}
}
