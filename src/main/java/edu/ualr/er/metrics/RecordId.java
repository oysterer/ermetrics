package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.RecordID
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * recordID is composed of recourdSource<separator>recordIndex
 * The source can be missing
 * THe separator character (default is period used by Oyster) 
 * The index is used for ID comparison and the source is ignored
 * 
 * Each property will always be either an empty string "" or a value
 * 
 * @author James True
 *
 */
public class RecordId {
	
	protected static final Logger log = LoggerFactory.getLogger(RecordId.class);

	protected String recordId;
	protected String recordSource;
	protected String recordTag;

	public RecordId() {
		this.recordId = "";
		this.recordSource = "";
		this.recordTag = "";
	}
	/**
	 * COnstruct the record ID from a string and delimiter
	 * If there is a single token then the recordID = recordIndex
	 * At most the value will be parsed into only two tokens
	 * 
	 * @param value of record ID
	 * @param delimiter between source and index
	 */
	public RecordId(String value, FieldDelimiter delimiter) {
		this();
		setRecordId(value, delimiter);
	}
	
	/**
	 * Construct the record ID and components using Oyster separator '.'
	 * @param value of record ID
	 */
	public RecordId(String value) {
		this();
		setRecordId(value, FieldDelimiter.PERIOD);
	}

	/**
	 * COnstruct the record ID from a string and delimiter
	 * If there is a single token then the recordID = recordIndex
	 * At most the value will be parsed into only two tokens
	 * 
	 * @param value of record ID
	 * @param delimiter between source and index
	 */
	public RecordId setRecordId(String value, FieldDelimiter delimiter) {
		this.recordId = value;
		// No value to set
		if (StringUtils.isBlank(value)) {
			setRecordSource(null);
			setRecordTag(null);
			return this;
		} 
		// No delimiter, just store the value as the tag
		if (FieldDelimiter.NONE.equals(delimiter)) {
			setRecordSource(null);
			setRecordTag(value);
			return this;
		}
		// Split on the delimiter and store the source and tag
		String[] tokens = value.split(delimiter.getRegexValue(),2);
		if (tokens.length == 2) {
			setRecordSource(tokens[0]);
			setRecordTag(tokens[1]);
		} else {
			setRecordSource(null);
			setRecordTag(value);
		}
		return this;
	}
	/*
	 * Use Oyster default ID delimiter (PERIOD)
	 */
	public RecordId setRecordId(String value) {
		setRecordId(value, FieldDelimiter.PERIOD);
		return this;
	}
	public String getRecordId() {
		return this.recordId;
	}
	
	/**
	 * Set the record ID from an array of tokens based on the index
	 * 
	 * @param tokens parsed from the record using the record delimiter
	 * @param col offset (1 - n) of the token containing the ID
	 * @param delimiter record ID delimiter
	 * @return self reference
	 */
	public RecordId setRecordId(String [] tokens, int col, FieldDelimiter delimiter) {
		String token = extractToken(tokens, col);
		setRecordId(token, delimiter);
		if (token == null) {
			log.error("RecordID column {} out of range, token count {}", col, tokens.length);
		}
		return this;
	}
	/*
	 * Use Oyster default ID delimiter (PERIOD)
	 */
	public RecordId setRecordId(String [] tokens, int col) {
		String token = extractToken(tokens, col);
		setRecordId(token);
		if (token == null) {
			log.error("RecordID column {} out of range, token count {}", col, tokens.length);
		}
		return this;
	}

	/*
	 * reference record source (optional)
	 * Prefix part of recordId = recordSource.recordNumber
	 */
	public String getRecordSource() {
		return this.recordSource;
	}
	protected RecordId setRecordSource(String value) {
		this.recordSource = clean(value);
		return this;
	}

	/*
	 * reference record id
	 * Suffix part of recordId = recordSource.recordNumber
	 */
	public String getRecordTag() {
		return recordTag;
	}
	protected RecordId setRecordTag(String value) {
		this.recordTag = clean(value);
		return this;
	}

	/**
	 * Trim the token of surrounding white space
	 * and un-escape quoted or escaped strings per CSV pattern
	 * 
	 * @param value
	 * @return modified string or empty string
	 */
	protected static String clean(String value) {
		if (StringUtils.isBlank(value)) {
			return "";
		}
		String result = value;
		if (value.startsWith("\"") && value.endsWith("\"")) {
			result = value.substring(1,value.length()-1);
		}
		return StringUtils.trimToEmpty(result);
	}

	/**
	 * extract a token based on the column
	 * @param tokens
	 * @param col
	 * @returns the token or null if the col < 1 or > token count
	 */
	protected static String extractToken(String [] tokens, int col) {
		// No reference
		if (col == 0)
			return "";
		// Cannot be negative
		if (col < 0)
			return null;
		// Adjust column number of array index
		int index = col - 1;
		if (index < tokens.length) {
			return tokens[index];
		}
		// Exceeds token count
		return null;
	}	
	@Override
	public String toString() {
		return this.recordId;
	}
}
