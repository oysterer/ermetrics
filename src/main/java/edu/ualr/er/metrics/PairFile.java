package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.PairFile
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract class representing a pair file
 * 
 * @author James True
 *
 */
public abstract class PairFile {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	
	public static long NO_LIMIT = 0l;
	
	private long written;
	private long limit;
	private String filename;
	private BufferedWriter writer;
	private boolean status;
	
	public PairFile(String filename, long limit) {
		this.written = 0L;
		this.limit = (limit == NO_LIMIT) ? Long.MAX_VALUE : limit;
		this.status = false;
		if (StringUtils.isBlank(filename)) {
			this.filename = null;
			log.error("No FileName specified");
			return;
		}
		this.filename = filename;
		try {
			this.writer = new BufferedWriter(new FileWriter(new File(this.filename)));
			this.writer.write(getPairHeader());
			this.status = true;
		} catch (IOException e) {
			log.error("Error Opening file: {}", this.filename);
		}
	}
	
	/**
	 * Is the output file open and ok
	 * @return true=opened, false=closed or error
	 */
	public boolean isStatusOk() {
		return this.status;
	}
	
	protected void setStatus(boolean status) {
		this.status = status;
	}
	
	/**
	 * Close the writer
	 * 
	 * This is required to prevent memory leaks. 
	 */
	public void close() {
		log.info("{} {} Pairs written", written, getPairType());
		if (this.writer != null) {
			try {
				this.writer.close();
			} catch (IOException e) {
				log.error("Error closing file: {}", this.filename);
			}
		}
		this.status = false;
	}
	
	/**
	 * Write a line to the file
	 * 
	 * @param pair to be written
	 */
	public void writePair(Pair pair) {
		if (pair == null) {
			log.error("Null pair parameter");
			return;
		}
		try {
			if (status && (written < limit)) {
				this.writer.write(getPairAsCsv(pair));
				++written;
			}
		} catch (IOException e) {
			// report the error and close the file
			log.info("Error Writing the {} File: {}", getPairType(), e.getMessage());
			close();
		}
	}

	
	/**
	 * Write all mismatched or matching pairs
	 * The list should be in key sequence
	 * 
	 * Iterate through the list and for all references with matching key values
	 * The compareLinks method determines whether the pair is counted and recorded
	 * 
	 * @param list
	 */
	public long writePairs(List<Link> list, boolean diagnosticMode) {

		// NPE protection and see if there is any work do to
		if ((list == null) || (list.size() == 0))
			return 0L;

		/*
		 * Each time the keyId changes we start a new group Because it is the first
		 * record there is nothing to compare it with If the next key is different then
		 * there are no pairs If there is more than on reference with the same keyID
		 * compare the previous one with the latest and write any mismatches
		 */
		long count = 0L;
		String keyId = null;
		int refBase = 0;
		for (int i = 0; i < list.size(); i++) {
			// get each reference
			Link ref = list.get(i);
			// If the keyId has changed start a new group
			if (!StringUtils.equals(keyId, getKey(ref))) {
				keyId = getKey(ref);
				refBase = i;
				// Nothing to compare the first one with
				continue;
			}
			// Compare everything in the group with this record
			for (int j = refBase; j < i; j++) {
				Link other = list.get(j);
				// Any compare=true return is counted and written out
				if (compareLinks(other,ref)) {
					++count;
					writePair(new Pair(other, ref));
					// If we have written the limit, stop
					if (!diagnosticMode && (written >= limit)) {
						return count;
					}
				}
			}
		}
		return count;
	}

	/**
	 * Override this to provide a descriptive name of the pair type
	 * 
	 * @return descriptive type name
	 */
	protected abstract String getPairType();
	
	/**
	 * return the CSV header line for the pair type
	 * 
	 * @return header line
	 */
	protected abstract String getPairHeader();
	
	/**
	 * Return the CSV line for the pair type
	 * Base implementation returns the tag portion of the record ID
	 * 
	 * @param pair to process
	 * @return detail line
	 */
	protected abstract String getPairAsCsv(Pair pair);
	
	/**
	 * Get the key value
	 * @param Link to extract
	 * @return String key value
	 */
	protected abstract String getKey(Link ref);
	
	/**
	 * Compare the links in two References
	 * This will cause all link pairs to be written out
	 * 
	 * @param left Link
	 * @param right Link
	 * @return true=match, false=different
	 */
	protected abstract boolean compareLinks(Link left, Link right);

}
