package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.Configuration
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * Configuration file, default is ERMetrics.properties
 * 
 * Values are:
 * title="title to be displayed at the beginning of the run"
 * ===== Link file information (file to evaluate)
 * The Line file name
 *	link.filename=Capture.link
 * Does the file have a header row true/false
 *	link.header=true
 * The file field separator is tab/comma/period/dash/pipe/space
 *	link.delimiter=tab
 * The source-index delimiter of the reference Id is tab/comma/period/dash/pipe/space
 * 	link.id.delimiter=period
 * The relative columns (1...n) containing the record reference IDs
 *	link.record.column=1
 * The relative column (0...n) containing the cluster ID
 *	link.cluster.column=2
 * The relative column (0...n) containing the ER rules used (0=ignore)
 *	link.rule.column=3
 * The relative column (0...n) containing the link filter value
 *  link.filter.column=0
 * The link filter value
 *  link.filter.value=
 * Write linked pairs to a file (convert clusters to pairs)
 * 	link.pair.filename=
 *
 * ===== Reference file information (reference clusters)
 * Name of the truth file
 *	reference.filename=TruthSet.csv
 * Is this a truth set true/false
 *	reference.is.truth=true
 * Does the file have a header row true/false
 *	reference.header=true
 * The file field separator is tab/comma/period/dash/pipe/space
 *	reference.delimiter=comma
 * The source-index delimiter of the reference Id is tab/comma/period/dash/pipe/space
 * 	reference.id.delimiter=none
 * The relative columns (1...n) containing the record reference IDs
 *	reference.record.column=1
 * The relative column (1...n) containing the cluster ID
 *	reference.cluster.column=2
 * The relative column (0...n) containing the reference filter value
 *  reference.filter.column=0
 * The reference filter value
 *  reference.filter.value=
 *
 * ===== Additional controls =====
 *
 * Outer Join true will evaluate all the link records 
 * 	which skews the results if link & truth don't contain the same records
 * Outer Join false will evaluate only the link records with record ID values contained in the truth file
 *	outer.join=false
 *  
 * Limit the number of errors before aborting the program 
 *	error.limit=30
 *
 * Enable diagnostic mode for additional information (runs longer, produces more output)
 * 	diagnostic.mode=off|[scan,...]
 *
 * ===== Output Controls
 * 
 * Append results to CSV file
 *  report.history.filename=
 *  
 * Write results as key=value pairs
 *  report.results.filename=
 
 * Write linked clusters a file
 * Invalid if the reference file is an encrypted truth set (.ert) file
 *	cluster.dump.filename=
 *
 * Log false positive link records to a file
 *	positive.pair.filename=
 * Limit of false positive records to write
 * 	positive.pair.limit=100
 *
 * Log false negative link records to a file
 * 	negative.pair.filename=
 * Limit of false negative records to write
 * 	negative.pair.limit=100
 * 
 * @author James True
 * 
 */
public class Configuration {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private String fileName;
	private Class<EncryptedTruth> encryptedTruthClass;
	
	private final PropertiesConfiguration config;
	private final List<String> diagnosticModes;

	public Configuration() {
		this(new PropertiesConfiguration());
	}

	public Configuration(String fileName) {
		this();
		setFileName(fileName);
	}

	@SuppressWarnings("unchecked")
	public Configuration(PropertiesConfiguration config) {
		this.config = config;
		this.config.setListDelimiter(',');
		this.diagnosticModes = new ArrayList<String>();
		try {
			this.encryptedTruthClass = (Class<EncryptedTruth>) Class.forName("edu.ualr.er.metrics.EncryptedTruthImpl");
		} catch (ClassNotFoundException e) {
			this.encryptedTruthClass = null;
		}
	}

	public String getFileName() {
		return this.fileName;
	}

	public Configuration setFileName(String fileName) {
		this.fileName = fileName;
		return this;
	}

	public boolean load() {
		return load(this.fileName);
	}

	/*
	 * Used to load configuration specified on command line from disk
	 */
	public boolean load(String fileName) {
		this.fileName = fileName;
		if (StringUtils.isBlank(this.fileName)) {
			log.error("Could not open Configuration File, no file name supplied");
			return false;
		}
		try {
			config.load(fileName);
		} catch (ConfigurationException e) {
			log.error("Could not load Configuration File {}", fileName);
			return false;
		}
		config.setFileName(this.fileName);
		setDiagnosticModes();
		// For now we use a fixed implementation name
		setEncryptedTruth("EncryptedTruthImpl");
		return true;
	}

	/*
	 * Used to load configuration saved in truth set EncryptedTruth file
	 */
	public boolean load(InputStream stream) {
		if (stream == null) {
			log.error("No stream supplied");
			return false;
		}
		try {
			config.load(stream);
		} catch (ConfigurationException e) {
			log.error("Could not load Configuration");
			return false;
		} finally {
			try {
				stream.close();
			} catch (IOException e) {
				log.warn("Error closing configuration stream: {}", e.getMessage());
			}
		}
		setDiagnosticModes();
		// For now we use a fixed implementation name
		setEncryptedTruth("EncryptedTruthImpl");
		return true;
	}

	/**
	 * Try to get the concrete EncryptedTruth class
	 */
	@SuppressWarnings("unchecked")
	protected void setEncryptedTruth(String className) {
		if (StringUtils.isBlank(className)) {
			this.encryptedTruthClass = null;
			log.debug("No EncryptedTruth implementation provided");
			return;
		}
		try {
			this.encryptedTruthClass = (Class<EncryptedTruth>) Class.forName("edu.ualr.er.metrics." + className);
		} catch (ClassNotFoundException e) {
			log.error("Could not resolve EncryptedTruth implementation");
			this.encryptedTruthClass = null;
		}
	}
	
	/*
	 * Get an instance object of the EncryptedTruth implementation class
	 */
	public EncryptedTruth encryptionFactory() {
		if (this.encryptedTruthClass == null) {
			return null;
		}
		try {
			return this.encryptedTruthClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			log.error("Could not instantiate EncryptedTruth");
		}
		return null;
	}
	
	/*
	 * Helper function for migrated properties 
	 * It maps from an old property name to the new property name
	 */
	protected void alias(String to, String from) {
		// No to key
		if (StringUtils.isBlank(to))
			return;
		// No from key
		if (StringUtils.isBlank(from))
			return;
		// No from value
		if (config.getProperty(from) == null)
			return;
		if (config.containsKey(to)) {
			log.debug("Cannot alias from {} to existing property {}={}", from, to, config.getProperty(to));
		}
		if (config.containsKey(from)) {
			Object obj = config.getProperty(from);
			config.setProperty(to, obj);
			config.clearProperty(from);
			log.debug("Aliased value {} from {} to {}", new Object[] {obj, from, to});
		}
	}
	
	/*
	 * Safely get and int property
	 * 
	 * @param key to retrieve
	 * @param missing missing/error value
	 * @return configured or missing value
	 */
	protected int getInt(String key, int missing) {
		int result;
		String value = config.getString(key);
		if (value == null) {
			log.debug("{} not specified default to {}", key, missing);
			return missing;
		}
		try {
			result = Integer.parseInt(value);
		} catch (Exception e) {
			result = missing;
			log.error("{} non-numeric value:'{}', must be 0-n, default to {}", key, value, missing);
		}
		return result;
	}
	
	/*
	 * Safely get an array of integers from the properties
	 * On any error return a valid array with a single value of 0
	 * 
	 * @param key to retrieve
	 * @param missing missing/error value
	 * @return configured or missing value
	 */
	protected int[] getIntArray(String key, int missing) {
		// Default result
		int [] results = new int[] {missing};
		if (StringUtils.isBlank(key)) {
			return results;
		}
		String [] values = config.getStringArray(key);
		if (values.length == 0) {
			return results;
		}
		results = new int[values.length];
		for (int i = 0; i < values.length; i++) {
			try {
				results[i] = Integer.parseInt(values[i]);
			} catch (Exception e) {
				results[i] = missing;
				log.error("{} non-numeric value:'{}', must be 0-n", key, values[i]);
			}
		}
		return results;
	}
	
	/*
	 * The title for this run
	 */
	public String getTitle() {
		String value = config.getString("title");
		return value;
	}
	public String getTitle(String alternate) {
		String value = config.getString("title");
		if (StringUtils.isBlank(value)) {
			value = alternate;
		}
		log.debug("Title:'{}'", value);
		return value;
	}

	/*
	 * Link file parameters
	 */
	private String linkFile = null;
	public String getLinkFile() {
		if (linkFile == null) {
			linkFile = config.getString("link.filename");
			log.debug("Link File Name:'{}'", linkFile);
		}
		return linkFile;
	}

	private Boolean linkHeader = null;
	public boolean isLinkHeader() {
		if (linkHeader == null) {
			linkHeader = config.getBoolean("link.header", true);
			log.debug("Link File Header:{}", linkHeader ? "Yes" : "No");
		}
		return linkHeader;
	}

	private FieldDelimiter linkDelimiter = null;
	public FieldDelimiter getLinkDelimiter() {
		if (linkDelimiter == null) {
			String name = config.getString("link.delimiter", "tab");
			linkDelimiter = FieldDelimiter.getDelimiterByName(name);
			if (linkDelimiter == null) {
				log.error("Invalid Link Delimiter:[{}] Options are: {}", name, FieldDelimiter.values());
				linkDelimiter = FieldDelimiter.NONE;
			} else {
				log.debug("Link Delimiter:{}", linkDelimiter.getName());
			}
		}
		return linkDelimiter;
	}

	private FieldDelimiter linkIdDelimiter = null;
	public FieldDelimiter getLinkIdDelimiter() {
		if (linkIdDelimiter == null) {
			String name = config.getString("link.id.delimiter", "period");
			linkIdDelimiter = FieldDelimiter.getDelimiterByName(name);
			if (linkIdDelimiter == null) {
				log.error("Invalid Link Record ID Delimiter:[{}] Options are: {}", name, FieldDelimiter.values());
				linkIdDelimiter = FieldDelimiter.PERIOD;
			} else {
				log.debug("Link Record ID Delimiter:{}", linkIdDelimiter.getName());
			}
		}
		return linkIdDelimiter;
	}

	private int[] linkRecordColumns = null;
	public int[] getLinkRecordColumns() {
		if (linkRecordColumns == null) {
			// Accommodate old property names
			alias("link.record.columns","link.recid.column");
			alias("link.record.columns","link.record.column");
			// Get the official property
			linkRecordColumns = getIntArray("link.record.columns",1);
			log.debug("Link Record ID Columns:{}", linkRecordColumns);
		}
		return linkRecordColumns;
	}

	private Integer linkClusterColumn = null;
	public int getLinkClusterColumn() {
		if (linkClusterColumn == null) {
			// Accommodate old property names
			alias("link.cluster.column","link.clusterid.column");
			// Get the official property
			linkClusterColumn = getInt("link.cluster.column",2);
			log.debug("Link Cluster Column:{}", linkClusterColumn);
		}
		return linkClusterColumn;
	}

	private Integer linkRuleColumn = null;
	public int getLinkRuleColumn() {
		if (linkRuleColumn == null) {
			linkRuleColumn = getInt("link.rule.column", 3);
			log.debug("Link Rule Column:{}", linkRuleColumn);
			if (linkRuleColumn == 0) {
				log.info("Link Rule Column ignored");
			}
		}
		return linkRuleColumn;
	}
	
	private Integer linkFilterColumn = null;
	public int getLinkFilterColumn() {
		if (linkFilterColumn == null) {
			linkFilterColumn = getInt("link.filter.column",0);
			log.debug("Link Filter Column:{}", linkFilterColumn);
		}
		return linkFilterColumn;
	}
	
	private String linkFilterValue = null;
	public String getLinkFilterValue() {
		if (linkFilterValue == null) {
			linkFilterValue = config.getString("link.filter.value");
			log.debug("Link Filter value:'{}'", linkFilterValue);
			if ((linkFilterValue == null) && (getLinkFilterColumn() > 0)) {
				log.error("Link Filter Value is missing and column is specified");
			}
			if ((linkFilterValue != null) && (getLinkFilterColumn() > 0)) {
				log.error("Link Filter Value is '{}' but column is not specified", linkFilterValue);
			}
		}
		return linkFilterValue;
	}
	
	public boolean hasLinkFilter() {
		return (getLinkFilterColumn() > 0) && (StringUtils.isNotBlank(getLinkFilterValue()));
	}
	
	private String linkPairFile = null;
	public String getLinkPairFile() {
		if (linkPairFile == null) {
			linkPairFile = config.getString("link.pair.filename");
			log.debug("Link Pair File Name:'{}'", linkPairFile);
		}
		return linkPairFile;
	}
	
	public int getMaxLinkColumn() {
		int max = getLinkRuleColumn();
		if (getLinkClusterColumn() > max) 
			max = getLinkClusterColumn();
		if (getLinkFilterColumn() > max)
			max = getLinkFilterColumn();
		for (int col : getLinkRecordColumns()) {
			if (col > max)
				max = col;
		}
		return max;
	}

	/*
	 * Reference (Truth) file parameters
	 */
	private String referenceFile = null;
	public String getReferenceFile() {
		if (referenceFile == null) {
			referenceFile = config.getString("reference.filename");
			log.debug("Reference File Name:'{}'", referenceFile);
		}
		return referenceFile;
	}

	private Boolean referenceTruth = null;
	public boolean isReferenceTruth() {
		if (referenceTruth == null) {
			referenceTruth = config.getBoolean("reference.truth", true);
		log.debug("Reference Truth Set:{}", referenceTruth ? "Yes" : "No");
		}
		return referenceTruth;
	}

	private Boolean referenceHeader = null;
	public boolean isReferenceHeader() {
		if (referenceHeader == null) {
			referenceHeader = config.getBoolean("reference.header", true);
			log.debug("Reference File Header:{}", referenceHeader ? "Yes" : "No");
		}
		return referenceHeader;
	}

	private FieldDelimiter referenceDelimiter = null;
	public FieldDelimiter getReferenceDelimiter() {
		if (referenceDelimiter == null) {
		String name = config.getString("reference.delimiter", "comma");
		referenceDelimiter = FieldDelimiter.getDelimiterByName(name);
		if (referenceDelimiter == null) {
			log.error("Invalid Reference Delimiter:[{}] Options are: {}", name, FieldDelimiter.values());
			referenceDelimiter = FieldDelimiter.NONE;
		} else {
			log.debug("Reference Delimiter:{}", referenceDelimiter.getName());
		}
		}
		return referenceDelimiter;
	}

	private FieldDelimiter referenceIdDelimiter = null;
	public FieldDelimiter getReferenceIdDelimiter() {
		if (referenceIdDelimiter == null) {
			String name = config.getString("reference.id.delimiter", "none");
			referenceIdDelimiter = FieldDelimiter.getDelimiterByName(name);
			if (referenceIdDelimiter == null) {
				log.error("Invalid Reference Record ID Delimiter:[{}] Options are: {}", name, FieldDelimiter.values());
				referenceIdDelimiter = FieldDelimiter.PERIOD;
			} else {
				log.debug("Reference Record ID Delimiter:{}", referenceIdDelimiter.getName());
			}
		}
		return referenceIdDelimiter;
	}

	private int[] referenceRecordColumns = null;
	public int[] getReferenceRecordColumns() {
		if (referenceRecordColumns == null) {
			// Accommodate old property names
			alias("reference.record.columns","reference.recid.column");
			alias("reference.record.columns","reference.reecord.column");
			alias("reference.record.columns","reference.record.column");
			// Get the official property
			referenceRecordColumns = getIntArray("reference.record.columns", 1);
			log.debug("Reference Record ID Columns:{}", referenceRecordColumns);
		}
		return referenceRecordColumns;
	}

	private Integer referenceClusterColumn = null;
	public int getReferenceClusterColumn() {
		if (referenceClusterColumn == null) {
			// Accommodate old property names
			alias("reference.cluster.column","reference.clusterid.column");
			// Get the official property
			referenceClusterColumn = getInt("reference.cluster.column", 2);
			log.debug("Reference Cluster Column:{}", referenceClusterColumn);
		}
		return referenceClusterColumn;
	}
	
	private Integer referenceFilterColumn = null;
	public int getReferenceFilterColumn() {
		if (referenceFilterColumn == null) {
			referenceFilterColumn = getInt("reference.filter.column",0);
			log.debug("Reference Filter Column:{}", referenceFilterColumn);
		}
		return referenceFilterColumn;
	}
	
	private String referenceFilterValue = null;
	public String getReferenceFilterValue() {
		if (referenceFilterValue == null) {
			referenceFilterValue = config.getString("reference.filter.value");
			log.debug("Reference Filter value:'{}'", referenceFilterValue);
			if ((referenceFilterValue == null) && (getReferenceFilterColumn() > 0)) {
				log.error("Reference Filter Value is missing and column is specified");
			}
			if ((referenceFilterValue != null) && (getReferenceFilterColumn() == 0)) {
				log.error("Reference Filter Value is '{}' but column is not specified",referenceFilterValue);
			}
		}
		return referenceFilterValue;
	}
	
	public boolean hasReferenceFilter() {
		return (getReferenceFilterColumn() > 0) && (StringUtils.isNotBlank(getReferenceFilterValue()));
	}
	
	public int getMaxReferenceColumn() {
		int max = getReferenceClusterColumn();
		if (getReferenceFilterColumn() > max)
			max = getReferenceFilterColumn();
		for (int col : getReferenceRecordColumns()) {
			if (col > max)
				max = col;
		}
		return max;
	}

	
	/*
	 * Whether to do an outer join (process all records)
	 */
	private Boolean outerJoin = null;
	public boolean isOuterJoin() {
		if (outerJoin == null) {
			outerJoin = config.getBoolean("outer.join", false);
			log.debug("Calculate {} references", outerJoin ? "all" : "only matching");
		}
		return outerJoin;
	}
	
	/**
	 * Get the list of diagnostic modes
	 * 
	 * @return list of modes
	 */
	public List<String> getDiagnosticModes() {
		return this.diagnosticModes;
	}
	
	/**
	 * Set the diagnostic modes from an array
	 * 
	 * @param modes to set
	 */
	public void setDiagnosticModes(String [] modes) {
		// Clear the list before starting
		diagnosticModes.clear();
		// Nothing to set
		if (modes == null)
			return;
		if (modes.length == 0)
			return;
		log.debug("Diagnostic Modes:{}", modes.length);
		
		// Set them all to lower case for comparison
		for (int i = 0; i < modes.length; i++) {
			String mode = modes[i].toLowerCase();
			diagnosticModes.add(mode);
			log.debug("\tMode:{}", mode);
		}
	}
	
	/**
	 * Set the diagnostic modes from the configuration property
	 */
	protected void setDiagnosticModes() {
		// Get the defined diagnostic modes
		String [] modes = config.getStringArray("diagnostic.mode");
		setDiagnosticModes(modes);
	}

	/**
	 * Diagnostic modes set to anything other than "off" or "false" returns true
	 * 
	 * @return	true=one or more diagnostic modes set
	 * 			false=diagnostics off
	 */
	public boolean isDiagnosticMode() {
		// No modes defined, not diagnostic mode
		if (diagnosticModes.size() == 0)
			return false;
		// If defined then return false
		if (isDiagnosticMode("false","off"))
			return false;
		return true;
	}
	
	/**
	 * See if any of the modes in the supplied list are configured
	 * @param modes to test
	 * @return true=defined, false=not defined or no list to test
	 */
	public boolean isDiagnosticMode(String ... modes) {
		// No modes defined
		if (diagnosticModes.size() == 0)
			return false;
		// NPE protection
		if (modes == null)
			return false;
		// No mode to test
		if (modes.length == 0)
			return false;
		// If the mode name (lower case) is in the list
		for (String mode: modes) {
			if (diagnosticModes.contains(mode.toLowerCase())) {
				return true;
			}
		}
		return false;
	}


	/*
	 * stop after this error limit
	 */
	private Integer errorLimit = null;
	public int getErrorLimit() {
		if (errorLimit == null) {
			errorLimit = getInt("error.limit", 30);
			log.debug("Error limit:{}", errorLimit);
		}
		return errorLimit;
	}

	/*
	 * Specified to log false positive link records to a file
	 */
	private String clusterDumpFile = null;
	public String getClusterDumpFile() {
		if (clusterDumpFile == null) {
			clusterDumpFile = config.getString("cluster.dump.filename");
			log.debug("New/False Positive PairFile Name:'{}'", clusterDumpFile);
		}
		return clusterDumpFile;
	}

	/*
	 * Specified to log false positive link records to a file
	 */
	private String positivePairFile = null;
	public String getPositivePairFile() {
		if (positivePairFile == null) {
			positivePairFile = config.getString("positive.pair.filename");
			log.debug("New/False Positive PairFile Name:'{}'", positivePairFile);
		}
		return positivePairFile;
	}

	/*
	 * Limit for the number of false positive pairs to output
	 */
	private Integer psitivePairLimit = null;
	public int getPositivePairLimit() {
		if (psitivePairLimit == null) {
			psitivePairLimit = getInt("positive.pair.limit", 100);
			log.debug("New/False Positive Pair Limit:{}", psitivePairLimit);
		}
		return psitivePairLimit;
	}

	/*
	 * Specified to log false negative link records to a file
	 */
	private String negativePairFile = null;
	public String getNegativePairFile() {
		if (negativePairFile == null) {
			negativePairFile = config.getString("negative.pair.filename");
			log.debug("New/False Negative Pair File Name:'{}'", negativePairFile);
		}
		return negativePairFile;
	}

	/*
	 * Limit for the number of false negative pairs to output
	 */
	private Integer negativePairLimit = null;
	public int getNegativePairLimit() {
		if (negativePairLimit == null) {
			negativePairLimit = getInt("negative.pair.limit", 100);
			log.debug("New/False Negative Pair Limit:{}", negativePairLimit);
		}
		return negativePairLimit;
	}

	/*
	 * Write results to a key=value pair file 
	 * Default=none
	 */
	private String reportResultsFilename = null;
	public String getReportResultsFilename() {
		if (reportResultsFilename == null) {
			reportResultsFilename = config.getString("report.results.filename");
			log.debug("Results Key=Value File Name:'{}'", reportResultsFilename);
		}
		return reportResultsFilename;
	}
	
	/*
	 * Append results to a CSV file
	 * Default=none
	 */
	private String reportHistoryFilename = null;
	public String getReportHistoryFilename() {
		if (reportHistoryFilename == null) {
			reportHistoryFilename = config.getString("report.history.filename");
			log.debug("Results History CSV File Name:'{}'", reportHistoryFilename);
		}
		return reportHistoryFilename;
	}

	/**
	 * Merge in saved parameters from encrypted truth set
	 * 
	 * @param override
	 */
	public void merge(Configuration override) {
		config.setProperty("reference.truth", override.isReferenceTruth());
		config.setProperty("reference.header", override.isReferenceHeader());
		config.setProperty("reference.delimiter", override.getReferenceDelimiter().getName());
		config.setProperty("reference.id.delimiter", override.getReferenceIdDelimiter().getName());
		config.setProperty("reference.record.columns", override.getReferenceRecordColumns());
		config.setProperty("reference.cluster.column", override.getReferenceClusterColumn());
		config.setProperty("outer.join", override.isOuterJoin());
		config.setProperty("error.limit", override.getErrorLimit());
		config.setProperty("positive.pair.filename", override.getPositivePairFile());
		config.setProperty("positive.pair.limit", override.getPositivePairLimit());
		config.setProperty("negative.pair.filename", override.getNegativePairFile());
		config.setProperty("negative.pair.limit", override.getNegativePairLimit());
	}
}