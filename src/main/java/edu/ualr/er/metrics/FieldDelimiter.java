package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.FieldDelimiter
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

/**
 * Enumeration of column delimiter values
 * 
 * @author James True
 *
 */

public enum FieldDelimiter {
	NONE("none",""), 
	COMMA("comma", ","), 
	PERIOD("period", "."), 
	TAB("tab", "\t"), 
	DASH("dash","-"), 
	PIPE("pipe","|"), 
	SPACE("space", " ");

	private final String name;
	private final String value;
	private final String regexValue;

	private FieldDelimiter(String name, String value) {
		this.name = name;
		this.value = value;
		this.regexValue = Pattern.quote(value);
	}

	/**
	 * Get the delimiter name
	 * 
	 * @return the name of the delimiter
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get the delimiter value
	 * 
	 * @return the value of the delimiter
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Get the delimiter value escaped for RegEx parsing
	 * 
	 * @return RegEx escaped pattern
	 */
	public String getRegexValue() {
		return regexValue;
	}

	/**
	 * Get the delimiter value as a character array
	 * 
	 * @return the value as a character array
	 */
	public char[] getValueAsChars() {
		return value.toCharArray();
	}

	/**
	 * Get the enumeration by the name (ignoring case)
	 * 
	 * @param name to match
	 * @return Enum or null if no match
	 */
	public static FieldDelimiter getDelimiterByName(String name) {
		for (FieldDelimiter entry : FieldDelimiter.values()) {
			if (StringUtils.equalsIgnoreCase(entry.name, name)) {
				return entry;
			}
		}
		return null;
	}

	/**
	 * Get the enumeration by value (ignoring case)
	 * 
	 * @param value to match
	 * @return Enum or null if no match
	 */
	public static FieldDelimiter getDelimiterByValue(String value) {
		for (FieldDelimiter entry : FieldDelimiter.values()) {
			if (StringUtils.equals(entry.value, value)) {
				return entry;
			}
		}
		return null;
	}
}
