package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.ReferenceCount
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.util.List;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generic class for count reference counts
 * 
 * Total number of references 
 * Number of reference pairs 
 * Number of reference clusters
 * 
 * @author James True
 *
 */
public class ReferenceCount {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * Total number of references
	 */
	private long count;
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}
	public void addCount(long count) {
		this.count += count;
	}

	/**
	 * The number of pairs
	 * Computed from the number of references in a cluster
	 */
	private long pairs;
	public long getPairs() {
		return pairs;
	}
	public void setPairs(long pairs) {
		this.pairs = pairs;
	}
	public void addPairs(long pairs) {
		this.pairs += pairs;
	}

	/**
	 * The number of unique ID values (clusters)
	 */
	private long clusters;
	public long getClusters() {
		return clusters;
	}
	public void setClusters(long clusters) {
		this.clusters = clusters;
	}
	public void addClusters(long clusters) {
		this.clusters += clusters;
	}

	/**
	 * No argument constructor
	 */
	public ReferenceCount() {
		this.count = 0;
		this.pairs = 0;
		this.clusters = 0;
	}

	/**
	 * Calculate the pairs for this object using its count value
	 */
	public void computePairs() {
		this.pairs = computePairs(this.count);
	}

	/**
	 * Calculate the number of pairs for a given number of items 
	 * This could be references, clusters, etc.
	 * 
	 * For use without instantiating the class
	 * 
	 * @param recs
	 * @return
	 */
	public static long computePairs(long recs) {
		long pairs = 0;
		if (recs > 1) {
			pairs = (recs * (recs - 1)) / 2;
		}
		return pairs;
	}

	/**
	 * Complete the previous (control break)
	 * This is called by computeClusters()
	 * 
	 * @param ID of group to close
	 * @param count count of items in the group
	 * 
	 * @return beginning reference count for next group
	 */
	protected long computeCluster(String ID, long count) {
		this.clusters++;
		long clusterPairs = computePairs(count);
		this.pairs += clusterPairs;
		log.debug("Group:{} Count:{} Pairs:{} ID:{}", new Object[] { this.clusters, count, clusterPairs, ID });
		return 0L;
	}

	/**
	 * Takes a sorted list of References 
	 * The list should already be sorted in key sequence 
	 * - Count the number if distinct LinkID values in the list 
	 * - For each cluster of matching LinkID values count the number of occurrences 
	 * - calculate the number of pairs for those occurrences
	 * 
	 * @param List containing the ID values to evaluate
	 * @param Comparator to select control cluster grouping
	 * 
	 * @return count of items in the list
	 */
	public long computeClusters(List<Link> list, Function<Link,String> getKey) {
		if ((list == null) || (list.size() == 0))
			return count;

		// Store the count of references
		this.count = list.size();

		// Save history
		String prevKey = null;
		long refCount = 0L;

		// Process each item in the list.
		for (Link ref : list) {
			// Ignore null records
			if (ref == null)
				continue;
			String key = getKey.apply(ref);
			if (StringUtils.isBlank(key))
				continue;
			// Prevent control break on first record
			if (prevKey == null)
				prevKey = key;
			// If references are not, compute the cluster
			if (!prevKey.equals(key)) {
				// Control break, finish previous item
				refCount = computeCluster(prevKey, refCount);
			}
			// Record this item
			++refCount;
			prevKey = key;
			log.trace("  Count:{} Item:{}", refCount, key);
		}

		// Finish last group
		computeCluster(prevKey, refCount);
		log.info("Total Count:{} Pairs:{} Clusters:{}", new Object[] { this.count, this.pairs, this.clusters });

		return this.count;
	}
}
