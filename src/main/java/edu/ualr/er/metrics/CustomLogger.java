package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.CustomLogger
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.filter.ThresholdFilter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
import ch.qos.logback.core.util.FileSize;

/*	
 *
 * Implementing this configuration
 * 
 *  <appender name="CONSOLE" class="ch.qos.logback.core.ConsoleAppender">
 *		<filter class="ch.qos.logback.classic.filter.ThresholdFilter">
 *			<level>INFO</level>
 *		</filter>
 *		<encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
 *			<Pattern>
 *				%msg%n
 *			</Pattern>
 *		</encoder>
 *	</appender>
 *	
 *	<appender name="FILE"
 *		class="ch.qos.logback.core.rolling.RollingFileAppender">
 *		<file>ERMetrics.log</file>
 *		<filter class="ch.qos.logback.classic.filter.ThresholdFilter">
 *			<level>INFO</level>
 *		</filter>
 *		<encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
 *			<Pattern>
 *				%d{yyyy-MM-dd HH:mm:ss} %-5level %logger{36} - %msg%n
 *			</Pattern>
 *		</encoder>
 *		<rollingPolicy
 *			class="ch.qos.logback.core.rolling.SizeAndTimeBasedRollingPolicy">
 *			<fileNamePattern>ERMetrics.%d{yyyy-MM-dd}.%i.log</fileNamePattern>
 *			<maxFileSize>10MB</maxFileSize>
 *			<maxHistory>60</maxHistory>
 *			<totalSizeCap>200MB</totalSizeCap>
 *		</rollingPolicy>
 *	</appender>
 *	
 *	<logger name="edu.ualr" level="INFO">
 *		<appender-ref ref="FILE" />
 *	</logger>
 *	
 *	<root level="INFO">
 *		<appender-ref ref="CONSOLE" />
 *	</root>
 *
 * @author James True
 *
 */

public class CustomLogger {
	
	private static String LOG_FILE_NAME = "ERMetrics.log";
	private static String LOG_FILE_DATE_NAME = "'ERMetrics.'yyyy-MM-dd.HH-mm-ss'.log'";
	private static String CONSOLE_LAYOUT_PATTERN = "%msg%n";
	//private static String FILE_LAYOUT_PATTERN = "%date %level [%thread] %logger{10} [%file:%line] %msg%n";
	private static String FILE_LAYOUT_PATTERN = "%date %level %msg%n";
	
	private static SimpleDateFormat dateFormatter = new SimpleDateFormat(LOG_FILE_DATE_NAME);

	/**
	 * 
	 * @param name of the logger
	 * @param file, log file name, or blank to generate date-named file, or null uses standard value 
	 * @param level, default is INFO
	 * @return
	 */
	public static Logger createLoggerFor(String name, String file, String levelName) {
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();

		// Default level to info
		if (StringUtils.isBlank(levelName)) {
			levelName = "INFO";
		}
		Level level = Level.toLevel(levelName.toUpperCase());

		/*
		 *  If the user specifies a file, use a normal file appender
		 *  otherwise use the rolling appender
		 */
		FileAppender<ILoggingEvent> fileAppender;
		if (file == null) {
			fileAppender = getRollingAppender(lc);
		} else {
			fileAppender = getFileAppender(lc, file);
		}

		Logger logger = (Logger) LoggerFactory.getLogger(name);
		logger.addAppender(getConsoleAppender(lc,level));
		logger.addAppender(fileAppender);
		logger.setLevel(level);
		logger.setAdditive(false); /* set to true if root should log too */

        Logger rootLogger = lc.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
        //rootLogger.detachAppender("console");
        rootLogger.addAppender(fileAppender);
        rootLogger.setLevel(level);
		
		return logger;
	}

	/**
	 * Console appender is defined in the configuration file
	 * 
	 * @param lc is the logging context
	 * @return the constructed and started console appender
	 */
	private static ConsoleAppender<ILoggingEvent> getConsoleAppender(LoggerContext lc, Level level) {
		ConsoleAppender<ILoggingEvent> appender = new ConsoleAppender<ILoggingEvent>();
		appender.setContext(lc);
		appender.setName("CONSOLE");
		appender.setEncoder(getEncoder(lc, CONSOLE_LAYOUT_PATTERN));
		if (level.isGreaterOrEqual(Level.ERROR)) {
			appender.addFilter(getFilter(lc,level.toString()));
		} else {
			appender.addFilter(getFilter(lc, "INFO"));
		}
		appender.start();
		return appender; 
	}

	/**
	 * File appender, 
	 * @param lc is the logging context
	 * @param name of the log file, a blank name creates a Date-Time named file
	 * @return the constructed and started file appender
	 */
	private static FileAppender<ILoggingEvent> getFileAppender(LoggerContext lc, String name) {
		
		// Establish the file name for the log
		String filename = name;
		if (StringUtils.isBlank(filename)) {
			filename = dateFormatter.format(new Date());
		}

		// Instantiate a new appender
		FileAppender<ILoggingEvent> appender = new FileAppender<ILoggingEvent>();
		appender.setContext(lc);
		appender.setName("FILE");
		appender.setFile(filename);
		appender.setEncoder(getEncoder(lc, FILE_LAYOUT_PATTERN));
		appender.addFilter(getFilter(lc, "DEBUG"));
		appender.setAppend(true);
		appender.start();
		return appender; 
	}

	/**
	 * Create a rolling file appender
	 * Appenders cannot be shared so a new instance is created
	 * A standard file name is used with a day rolling appender
	 * The output is also size limited
	 * 
	 * @param lc is the logging context
	 * @return the constructed and started appender
	 */
	private static FileAppender<ILoggingEvent> getRollingAppender(LoggerContext lc) {
		
		// Establish the log file name and name components
		String filename = LOG_FILE_NAME;
		String basename = FilenameUtils.getBaseName(filename);
		String extension = FilenameUtils.getExtension(filename);

		RollingFileAppender<ILoggingEvent> appender = new RollingFileAppender<ILoggingEvent>();
		appender.setContext(lc);	
		appender.setName("FILE");
		appender.setFile(filename);
		appender.setEncoder(getEncoder(lc, FILE_LAYOUT_PATTERN));
		appender.addFilter(getFilter(lc, "DEBUG"));
		appender.setAppend(true);
		
		// Size and Time rolling policy, new file each day
		//SizeAndTimeBasedRollingPolicy<ILoggingEvent> policy = new SizeAndTimeBasedRollingPolicy<ILoggingEvent>();
		TimeBasedRollingPolicy<ILoggingEvent> policy = new TimeBasedRollingPolicy<ILoggingEvent>();
		policy.setContext(lc);
		policy.setParent(appender);
		policy.setFileNamePattern(basename + ".%d{yyyy-MM-dd}." + extension);
		//policy.setFileNamePattern(basename + ".%d{yyyy-MM-dd}.%i." + extension);
		//policy.setMaxFileSize(FileSize.valueOf("100MB"));
		policy.setTotalSizeCap(FileSize.valueOf("5GB"));
		policy.setMaxHistory(50);
		policy.start();
		
		appender.setRollingPolicy(policy);
		appender.start();
		return appender;
	}
	
	/**
	 * Create a level threshold filter
	 * Filters cannot be shared so a new instance is created
	 * 
	 * @param lc is the logging context
	 * @param level of the filter ERROR | WARN | INFO | DEBUG
	 * @return the constructed and started filter
	 */
	private static ThresholdFilter getFilter(LoggerContext lc, String level) {
		ThresholdFilter filter = new ThresholdFilter();
		filter.setContext(lc);
		filter.setLevel(level);
		filter.start();
		return filter;
	}
	
	/**
	 * Create a logging message layout encoder
	 * Encoders cannot be shared so a new instance is created
	 * 
	 * @param lc the logging context
	 * @param pattern of the message
	 * @return the constructed and started encoder
	 */
	private static PatternLayoutEncoder getEncoder(LoggerContext lc, String pattern) {
		PatternLayoutEncoder ple = new PatternLayoutEncoder();
		ple.setContext(lc);
		ple.setPattern(pattern);
		ple.start();
		return ple;
	}
}
