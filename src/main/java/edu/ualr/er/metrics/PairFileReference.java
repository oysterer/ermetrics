package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.PairFileReference
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Reference Pair file generated from the consolidated link information
 * 
 * THis is not currently used and would required a reference.pair-filename configuration
 * parameter and associated logic following parsing of the input
 * 
 * @author James True
 *
 */
public class PairFileReference extends PairFile {

	public PairFileReference(String filename, long limit) {
		super(filename, limit);
	}
	
	/**
	 * Override this to provide a descriptive name of the pair type
	 * 
	 * @return descriptive type name
	 */
	@Override
	protected String getPairType() {
		return "Referemce";
	}
	
	/**
	 * return the CSV header line for the pair type
	 * 
	 * @return header line
	 */
	@Override
	protected String getPairHeader() {
		return "LeftId,RightId\n";
	}
	
	/**
	 * Return the CSV line for the pair type
	 * Base implementation returns the tag portion of the record ID
	 * 
	 * @param pair to process
	 * @return detail line
	 */
	@Override
	protected String getPairAsCsv(Pair pair) {
		StringBuilder sb = new StringBuilder();
		Link leftLink = pair.getLeftLink();
		if (leftLink != null) {
			sb.append("\"").append(leftLink.getRecordTag()).append("\"").append(",");
		} else {
			sb.append(",");
		}
		Link rightLink = pair.getRightLink();
		if (rightLink != null) {
			sb.append("\"").append(rightLink.getRecordTag()).append("\"").append("\n");
		} else {
			sb.append("\n");
		}
		return sb.toString();
	}
	
	/**
	 * Get the key value
	 * @param Link to extract
	 * @return String key value
	 */
	@Override
	protected String getKey(Link ref) {
		return ref.getReferenceId();
	}
	
	/**
	 * Compare the links in two References
	 * This will cause all link pairs to be written out
	 * 
	 * @param left Link
	 * @param right Link
	 * @return true=match, false=different
	 */
	@Override
	protected boolean compareLinks(Link left, Link right) {
		return left.getReferenceId().equals(right.getReferenceId());

	}
}
