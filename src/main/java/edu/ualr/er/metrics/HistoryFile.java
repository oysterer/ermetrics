package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.HistoryFile
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents the history file created based on the
 * report.history.filename= configuration parameter
 * 
 * @author James True
 *
 */
public class HistoryFile extends ResultsFile {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	
	public HistoryFile(String filename, boolean append) {
		super(filename, append);
	}
	
	/**
	 * return the CSV header line for the pair type
	 * 
	 * @return header line
	 */
	@Override
	protected String getHeader() {
		StringBuilder sb = new StringBuilder(256);
		sb.append("Date,");
		sb.append("Time,");
		sb.append("Reference,");
		sb.append("Recs,");
		sb.append("EC,");
		sb.append("LC,");
		sb.append("AC,");
		sb.append("D,");
		sb.append("E,");
		sb.append("L,");
		sb.append("A,");
		sb.append("TP,");
		sb.append("FP,");
		sb.append("TN,");
		sb.append("FN,");
		sb.append("FPRate,");
		sb.append("FNRate,");
		sb.append("Accuracy,");
		sb.append("Precision,");
		sb.append("Recall,");
		sb.append("F-Measure,");
		sb.append("TWi\n");
		return sb.toString();
	}
	
	/**
	 * Return the CSV line for the pair type
	 * 
	 * @return detail line
	 */
	@Override
	protected String getResults(ERMetricsValues values, String reference) {
		StringBuilder sb = new StringBuilder();
		sb.append(new SimpleDateFormat("yyyy-MM-dd").format(new Date())).append(",");
		sb.append(new SimpleDateFormat("HH:mm:ss").format(new Date())).append(",");
		sb.append(StringUtils.wrapIfMissing(reference,'"')).append(",");
		sb.append(values.getRecs()).append(",");
		sb.append(values.getEC()).append(",");
		sb.append(values.getLC()).append(",");
		sb.append(values.getAC()).append(",");
		sb.append(values.getD()).append(",");
		sb.append(values.getE()).append(",");
		sb.append(values.getL()).append(",");
		sb.append(values.getA()).append(",");
		sb.append(values.getTP()).append(",");
		sb.append(values.getFP()).append(",");
		sb.append(values.getTN()).append(",");
		sb.append(values.getFN()).append(",");
		sb.append(values.getFPR()).append(",");
		sb.append(values.getFNR()).append(",");
		sb.append(values.getAccuracy()).append(",");
		sb.append(values.getPrecision()).append(",");
		sb.append(values.getRecall()).append(",");
		sb.append(values.getFMeasure()).append(",");
		sb.append(values.getTwi()).append("\n");
		return sb.toString();
	}
}
