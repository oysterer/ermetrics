
package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.ERMetricsReport
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

//import java.security.InvalidParameterException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Produce a report of metrics for an Oyster Run or any ER process that process
 * that has a link set file and a reference set file
 * 
 * The reference set can be either a previous link run or a truth set as needed
 * This determines how the calculations are presented.
 * 
 * This was migrated from a .NET MFC applications
 * 
 * @author James True
 *
 */
public class ERMetricsReport implements Runnable {

	private Logger log  = LoggerFactory.getLogger(this.getClass());

	private String configFileName;
	public String getConfigFileName() {
		return configFileName;
	}
	public void setConfigFileName(String configFileName) {
		this.configFileName = configFileName;
	}

	/*
	 * Create an encrypted truth set
	 */
	private boolean encryptTruth;
	public boolean isEncryptTruth() {
		return encryptTruth;
	}
	public void setEncryptTruth(boolean encryptTruth) {
		this.encryptTruth = encryptTruth;
	}
	
	/*
	 * Title for the run
	 */
	private String title;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = StringUtils.trimToEmpty(title);
	}
	
	private String version;
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}

	/*
	 * Holds the counts and calculates metrics
	 */
	private final ERMetricsValues values;
	public ERMetricsValues getValues() {
		return this.values;
	}

	/*
	 * Collection to hold link and reference data
	 * RecordID is the source record key so it must always be unique
	 * It is initialized by reading the link and reference data-sets
	 *
	 * This map contains combined data from: 
	 * 	The Link set [[Source.]RecID] [ClusterID] [Rules] The
	 * 	Reference set [RecID] [ReferenceID]
	 * 
	 * There will be only one reference per RecID 
	 * so a simple hash map is used and it will hold an outer join
	 * result set of all link and reference records
	 */
	private final Map<String, Link> linkResults;

	/* 
	 * Link data list
	 * Once the two files are joined we do not need the
	 * key and a list is more efficient and can be sorted
	 * which is what we need to do the work.
	 */
	private final List<Link> joinedResults;
	
	private final Configuration config;

	/**
	 * No argument constructor
	 */
	public ERMetricsReport() {
		this(new Configuration());
	}

	/**
	 * Constructor taking configuration
	 */
	public ERMetricsReport(Configuration config) {
		// Initialize the empty Results maps
		this.linkResults = new HashMap<String, Link>();
		
		// Default joined results to original list
		this.joinedResults = new ArrayList<Link>();
		
		// Initialize the metrics object
		this.values = new ERMetricsValues();
		
		// Initialize the options
		this.encryptTruth = false;
		this.title = null;
		
		// Configuration options
		this.config = config;
	}

	/**
	 * This is where all the actual work is done
	 */
	@Override
	public void run() {

		// If we have a config filename try to load it
		if (StringUtils.isNotBlank(this.configFileName)) {
			// Open and load the configuration file or fail
			if (!config.load(this.configFileName)) {
				return;
			}
		}
		// If there was not title on the command line, check the config file
		if (StringUtils.isBlank(this.title)) {
			setTitle(config.getTitle("ER Metrics Report"));
		}
		log.info(StringUtils.center(" "+getTitle()+" ",60,'='));
		if (this.version != null)
			log.info("ER Metrics version: {}", this.version);
		String javaVersion = System.getProperty("java.version");
		if (javaVersion != null)
			log.info("Java version: {}", javaVersion);
		log.info("Using configuration file {}",this.configFileName);
		log.info("Creating report ...");
		if (computeMetrics(values)) {
			outputResults(values, config.getReportResultsFilename(), getTitle());
			outputHistory(values, config.getReportHistoryFilename(), getTitle());
			outputMetrics(values, config.isReferenceTruth());
			log.info("Complete");
		} else {
			log.info("Ended with errors");
		}
	}

	private boolean computeMetrics(ERMetricsValues values) {
		
		// Clear the results collection in case of reuse
		joinedResults.clear();

		log.info("Calculating Entity Resolution ERMetrics . . .");

		// Get the error limit
		int errorLimit = config.getErrorLimit();
		
		// Read the Reference Set
		if (!readReferenceSet(config.getReferenceFile(), errorLimit, linkResults))
			return false;
		
		// Is the encrypt truth command line flag set?
		if (isEncryptTruth()) {
			String properties = config.getFileName();
			String zip = config.getReferenceFile() + ".ert";
			EncryptedTruth encryptor = config.encryptionFactory();
			if (encryptor != null) {
				encryptor.setFileName(zip).encrypt(config.getReferenceFile(), properties);
			} else {
				log.error("Truth set encruyption is not supported");
			}
		}

		// Read the link file
		if (!readLinkSet(config.getLinkFile(), errorLimit, linkResults))
			return false;
		
		// Join the linked results as indicated
		joinedResults.addAll(join(linkResults, config.isOuterJoin()));
		
		// Output CLuster file if requested
		dumpClusters(joinedResults,config.getClusterDumpFile());

		// Calculate Totals directly from joinedResults
		ReferenceCount total = values.getTotal();
		total.setCount(joinedResults.size());
		total.computePairs();

		// Get the ReferenceCount for E and EC counts
		ReferenceCount reference = values.getReference();
		// Prepare the list and compute
		computeTruePairs(joinedResults,reference); 

		// Get the ReferenceCount for L and LC counts
		ReferenceCount linked = values.getLinked();
		// Prepare the list and compute
		computeLinkedPairs(joinedResults,linked);

		// Get the ReferenceCount for TP (associated) counts
		ReferenceCount associated = values.getAssociated();
		// Prepare the list and compute
		computeAgreementPairs(joinedResults,associated);

		return true;
	}

	/**
	 * Output the metric results
	 * 
	 * @param truthSet is the reference set a truth set?
	 */
	private void outputMetrics(ERMetricsValues values, boolean truthSet) {
		
		log.info("");
		log.info("Total Distinct Records ID values (N) ............... = {}", values.getRecs());
		log.info("---------------------Clusters ----------------------");
		log.info("Reference Set Clusters (EC) ........................ = {}", values.getEC());
		log.info("Link Set Clusters (LC) ............................. = {}", values.getLC());
		log.info("Reference+Link Set Agreement Clusters (AC) ......... = {}", values.getAC());
		log.info("");
		log.info("---------------------Pairs -------------------------");
		log.info("Total Pairs (D) = ((N * N-1) / 2) .................. = {}", values.getD());
		log.info("Reference Set Pairs (E) ............................ = {}", values.getE());
		log.info("Link Set Linked Pairs (L) .......................... = {}", values.getL());
		log.info("Reference+Link Set Agreement Pairs (A) ............. = {}", values.getA());

		if (truthSet)
			outputTruthMetrics(values);
		else
			outputRelativeMetrics(values);
	}

	/**
	 * Output the metric results for a truth set
	 * 
	 * @param truthSet is the reference set a truth set?
	 */
	private void outputTruthMetrics(ERMetricsValues values) {
		log.info("True Positive Pairs (TP) ........................... = {}", values.getTP());
		log.info("False Positive Pairs (FP) = L - TP ................. = {}", values.getFP());
		log.info("True Negative Pairs (TN) = D - TP - FP - FN ........ = {}", values.getTN());
		log.info("False Negative Pairs (FN) = E - TP ................. = {}", values.getFN());
		log.info("");
		log.info("---------------------Rates - (0.0 - 1.0) -----------");
		log.info("False Positive Rate = FP / (TN + FP) ............... = {}", values.getFPR());
		log.info("False Negative Rate = FN / E ....................... = {}", values.getFNR());
		log.info("Accuracy = (TP + TN) / D ........................... = {}", values.getAccuracy());
		log.info("Precision = TP / (TP + FP) ......................... = {}", values.getPrecision());
		log.info("Recall = TP / (TP + FN) ............................ = {}", values.getRecall());
		log.info("F-Measure .......................................... = {}", values.getFMeasure());
		log.info("TWi ................................................ = {}", values.getTwi());
	}
	

	/**
	 * Output the metric results for a reference set
	 * 
	 * @param truthSet is the reference set a truth set?
	 */
	private void outputRelativeMetrics(ERMetricsValues values) {
		log.info("New positive pairs (NP) = L - A .................... = {}", values.getFP());
		log.info("New negative Pairs (NN) = E - A .................... = {}", values.getFN());
		log.info("Previous negative pairs (PN) = D - A - NP - NN ..... = {}", values.getTN());
		log.info("");
		log.info("-------------------- Rates - (0.0 - 1.0) -----------");
		log.info("New Positive Rate = NP / (PN + NP) ................. = {}", values.getFPR());
		log.info("New Negative Rate = NN / E ......................... = {}", values.getFNR());
		log.info("Relative Accuracy = (A + PN) / D ................... = {}", values.getAccuracy());
		log.info("Relative Precision = A / (A + NP) .................. = {}", values.getPrecision());
		log.info("Relative Recall = A / (A + NN) ..................... = {}", values.getRecall());
		log.info("Relative F-Measure ................................. = {}", values.getFMeasure());
		log.info("TWi .................................................= {}", values.getTwi());
	}
	
	private void outputResults(ERMetricsValues values, String filename, String title) {
		// No file to write
		if (StringUtils.isBlank(filename)) {
			return;
		}
		log.info("Writing Results to file:{}",filename);
		ResultsFile file = new ResultsFile(filename,false);
		if (file.isStatusOk()) {
			file.writeResults(values, title);
			file.close();
		}
	}
	
	private void outputHistory(ERMetricsValues values, String filename, String title) {
		// No file to write
		if (StringUtils.isBlank(filename)) {
			return;
		}
		log.info("Writing Results History to file:{}",filename);
		HistoryFile file = new HistoryFile(filename,true);
		if (file.isStatusOk()) {
			file.writeResults(values, title);
			file.close();
		}
	}
	
	private void dumpClusters(List<Link> joinedResults, String filename) {
		if (StringUtils.isBlank(filename)) {
			log.debug("No Cluster dump requested");
			return;
		}
		if (EncryptedTruth.isFileEncrypted(config.getReferenceFile())) {
			log.error("Cannot dump clusters with encrypted reference/truth set");
			return;
		}
		log.info("Dumping clusters to file: {}",filename);
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(filename),false))) {
			writer.write(Link.getLinkHeader());
			for (Link link: joinedResults) {
				writer.write(link.getLinkAsCSV());
			}
		} catch (IOException e) {
			log.error("Error writing Cluster output file: {}", filename);
		}
	}
	
	/*
	 * Create a list of all Reference Set LinkIDs Call CountClusters() on the list
	 * 
	 * Updates: 
	 * 	E - Equivalent Pairs - number of pairs for all Truth clusters 
	 * 	EC - Equivalent Clusters - number of distinct Truth LinkIDs (clusters)
	 * 
	 * If this is a link set rather than a truth set there equivalence has no
	 * real meaning
	 * 
	 */
	private void computeTruePairs(List<Link> list, ReferenceCount count) {
		log.info("Computing True Pairs and Clusters for {} references", list.size());

		// Sort the list
		list.sort(Link::compareReferenceId);
		// Compute the cluster statistics
		count.computeClusters(list, Link::getReferenceId);
		
		// Does the user want a False Negative pair file?
		String filename = config.getNegativePairFile();
		if (StringUtils.isBlank(filename)) {
			log.debug("No False Negative output requested");
			return;
		}
		// Instantiate the file
		PairFile fnFile = new PairFileFN(filename, config.getNegativePairLimit());
		if (!fnFile.isStatusOk()) {
			log.error("Could not open False Negative output file");
			return;
		}
		boolean diagnosticMode = config.isDiagnosticMode();
		Long fnCount = fnFile.writePairs(list,diagnosticMode);
		if (diagnosticMode) {
			log.info("False Negative Count:{}",fnCount);
		}
		// Close the file
		fnFile.close();
		return;
	}

	/*
	 * Create a list of all Link set LinkIDs Call CountClusters() on the list
	 * 
	 * Updates: 
	 * 	L Linked Pairs - number of pairs for all clusters 
	 * 	LC Linked Clusters - number of distinct Linked LinkIDs (clusters)
	 * 
	 * Returns: Record count
	 */
	private void computeLinkedPairs(List<Link> list, ReferenceCount count) {
		log.info("Computing Linked Pairs and Clusters for {} references", list.size());

		// Sort the list
		list.sort(Link::compareLinkId);		
		// Use CountClusters to determine the counts
		count.computeClusters(list,Link::getLinkId);
		
		// Does the user want a False Positive pair file
		String fpFilename = config.getPositivePairFile();
		if (!StringUtils.isBlank(fpFilename)) {
			// Instantiate the file
			PairFile fpFile = new PairFileFP(fpFilename, config.getPositivePairLimit());
			if (fpFile.isStatusOk()) {
				// Write the False Positive pairs
				boolean diagnosticMode = config.isDiagnosticMode();
				long fpCount = fpFile.writePairs(list,diagnosticMode);
				if (diagnosticMode) {
					log.info("False Positive Count:{}",fpCount);
				}
				// Close the file
				fpFile.close();
			} else {
				log.error("Could not open False Positive output file");
				return;
			}		
		} else {
			log.debug("No False Positive output requested");
		}
		
		// Does the user want a linked pair file
		String lpFilename = config.getLinkPairFile();
		if (!StringUtils.isBlank(lpFilename)) {
			// Instantiate the file
			PairFile lpFile = new PairFileLink(lpFilename, PairFile.NO_LIMIT);
			if (lpFile.isStatusOk()) {
				// Write the Linked Pairs
				boolean diagnosticMode = config.isDiagnosticMode();
				long lpCount = lpFile.writePairs(list,diagnosticMode);
				if (diagnosticMode) {
					log.info("Linked Pair Count:{}",lpCount);
				}
				// Close the file
				lpFile.close();
			} else {
				log.error("Could not open Linked Pair output file");
				return;
			}		
		} else {
			log.debug("No Linked Pair output requested");
		}
		
		return;
	}

	/*
	 * Create a list of all LinkIDs + ReferenceIDs 
	 * Count clusters of OysterID and ReferenceDD, calculate pairs
	 * 
	 * Updates: 
	 * 	TP - Agreement Pairs - number of agreement pairs for all clusters 
	 * 	AC - Agreement Clusters - the number of distinct LinkId+ReferenceID clusters
	 * 
	 * Returns: Record count
	 */
	private void computeAgreementPairs(List<Link> list, ReferenceCount count) {
		log.info("Computing Agreement Pairs and Clusters for {} references", list.size());

		// Sort the list
		list.sort(Link::compareAgreement);		
		// Use CountClusters to determine the counts
		count.computeClusters(list,Link::getAgreement);
		return;
	}

	private boolean readReferenceSet(String fileName, int errorLimit, Map<String, Link> refList) {
		if (StringUtils.isBlank(fileName)) {
			log.error("No Reference file specified");
			return false;
		}
		log.info("Reference file: {}",fileName);
		if (EncryptedTruth.isFileEncrypted(fileName)) {
			return readReferenceErt(fileName, errorLimit, refList);
		} else {
			return readReferenceFile(fileName, errorLimit, refList);
		}
	}
	
	/*
	 * Read the TruthSet file 
	 */
	private boolean readReferenceFile(String fileName, int errorLimit, Map<String, Link> refList) {
		try (Reader reader = new FileReader(new File(fileName))) {
			return parseReferenceSet(reader, errorLimit, refList);
		} catch (FileNotFoundException e) {
			log.error("Reference file {} not found", fileName);
			return false;
		} catch (Exception e) {
			log.error("Could not read reference file '{}'", fileName, e);
			return false;
		}
	}
	
	/*
	 * Read the TruthSet file 
	 */
	private boolean readReferenceErt(String fileName, int errorLimit, Map<String, Link> refList) {
		EncryptedTruth decryptor = config.encryptionFactory();
		if (decryptor == null ) {
			log.error("Encrypted Truth set not supported");
			return false;
		}
		InputStream stream = decryptor.setFileName(fileName).getReferenceStream(config);
		try (Reader reader = new InputStreamReader(stream, "UTF-8")) {
			return parseReferenceSet(reader, errorLimit, refList);
		} catch (UnsupportedEncodingException e) {
			log.error("UTF-8 Unsupported Encoding");
			return false;
		} catch (Exception e) {
			log.error("Could not read reference file '{}' {}", fileName, e.getMessage());
			return false;
		}
	}
	
	/*
	 * Read the TruthSet file This is read first and creates the initial set of Link
	 * Result records It does not assume unique RecID values, but having duplicates
	 * is not really a good thing.
	 *
	 * The Create Parameter controls whether new entries are created if not matched.
	 * It will ignore header line based on configuration parameters
	 * 
	 * Expected file format is two tokens Record ID Link ID Token delimiter is from
	 * the configuration file
	 */
	private boolean parseReferenceSet(Reader reader, int errorLimit, Map<String, Link> refList) {
		if (reader == null) {
			log.error("Missing Reference File Reader");
			return false;
		}
		if (refList == null) {
			log.error("Missing Reference List");
			return false;
		}
		boolean result = true;
		boolean diag = config.isDiagnosticMode("reference");
		FieldDelimiter delim = config.getReferenceDelimiter();
		if (FieldDelimiter.NONE.equals(delim)) {
			log.error("Reference delimiter is invalid, processing terminated");
			return false;
		}
		FieldDelimiter idDelim = config.getReferenceIdDelimiter();
		int[] recordCols = config.getReferenceRecordColumns();
		int clusterCol = config.getReferenceClusterColumn(); 
		log.info("Reference record.columns:{} cluster.column:{}", recordCols, clusterCol);
		int filterCol = config.getReferenceFilterColumn();
		String filterValue = null;
		if (filterCol > 0) {
			filterValue = config.getReferenceFilterValue();
			log.info("Reference filtering on value:'{}' in column:{}",filterValue,filterCol);
		}
		int minTokens = config.getMaxReferenceColumn();
		int input = 0;
		int filtered = 0;
		int errors = 0;
		int links = 0;
		int created = 0;
		int duplicate = 0;
		Map<String,List<String>> tuples = new HashMap<String,List<String>>();

		try (BufferedReader in = new BufferedReader(reader)) {
			String s;
			// Get the header line if present
			if (config.isReferenceHeader()) {
				s = in.readLine();
				log.info("\tHeader:[{}]", s);
			}
			// Read all the data lines
			while ((s = in.readLine()) != null) {
				// Blank line?
				if (s.trim().length() == 0) {
					continue;
				}
				// Count the input records
				++input;
				// Split the line on the specified delimiter
				String[] tokens = s.split(delim.getValue(), -1);
				// Display the first record for reference
				if (input == 1) {
					log.info("\tRecord #1:{}", String.join(":", tokens));
				}
				if (tokens.length < minTokens) {
					++errors;
					log.info("Invalid Reference record [{}]", s);
					// In the first few records, abort
					if (errors > errorLimit) {
						log.info("Aborting ...");
						result = false;
						break;
					} 
					// No further process of this record possible
					continue;
				}
				// Filter out all records not matching the filter value
				if (!filterMatch(tokens, filterValue, filterCol)) {
					++filtered;
					continue;
				}

				/**
				 * Modes if ClusterID is supplied
				 * 	1. Single Record ID column with referenceId column
				 * 		Match the record ID against existing record ID from truth set
				 * 			IF matched, store referenceID
				 * 			If not matched, create new reference
				 *  2. Multiple Record ID column with referenceId column
				 * 		Match each record ID sequentially against existing record ID from truth set
				 * 			IF matched, store referenceId
				 * 			If not matched, create new reference
				 *  3, Multiple Record ID column without referenceId column (requires transitive closure)
				 *  	Match any record ID with existing record ID
				 *  		If any matched they must all have the same pseudo referenceID
				 *  			Use the referenceId from those references to create any missing references
				 *  		If not matched, use the next pseudoL referenceId to create the references
				 *  
				 *  Duplicates references are counted and the referenceID is updated
				 */
				if (clusterCol > 0) {
					/*
					 * Process recordIDs sequentially if we have a referenceId
					 */
					for (int recordCol : recordCols) {
						Link ref = new Link();
						++links;
						ref.setRecordId(tokens,recordCol, idDelim);
						ref.setReferenceId(tokens,clusterCol);
						String recId = ref.getRecordTag();
						if (refList.containsKey(recId)) {
							Link link = refList.get(recId);
							++duplicate;
							if (diag) {
								log.debug("Matched [{}] [{}] [{}]", link.getRecordTag(), link.getReferenceId(), ref.getReferenceId());
							}
							// Save last value
							link.setReferenceId(ref.getReferenceId());
						} else {
							++created;
							if (diag) {
								log.debug("Created [{}] [{}]", ref.getRecordTag(), ref.getReferenceId());
							}
							refList.put(ref.getRecordTag(), ref);
						}
					}
				} else {
					/*
					 *  Process recordIDs as a group to create tuples for transitive closure
					 *  This creates a tuples map which is used to close all the pairs 
					 *  into the refList
					 */
					String [] pairIds = new String[recordCols.length];
					// Extract all the Ids to the tuple set
					RecordId id = new RecordId();
					for (int i = 0; i < recordCols.length; i++) {
						pairIds[i] = id.setRecordId(tokens,recordCols[i], idDelim).getRecordTag();
					}
					// Save them in the tuples map for transitive closure
					savePairs(tuples, pairIds, diag);
				}
			}
			
			/*
			 *  If we processed pairs then we need to perform transitive closure on the tuples map
			 *  to create the closure map and then add all those entries to the refList
			 */
			if ((clusterCol == 0) && tuples.size() > 0) {
				// Perform transitive closure on all the pairs
				Map<String, String> closure = closePairs(tuples,diag);
				for (Map.Entry<String, String> entry : closure.entrySet()) {
					// In this list the recordID is only the recordTag
					String recId = entry.getKey();
					String refId = entry.getValue();
					Link link = new Link();
					++links;
					link.setRecordId(recId);
					link.setReferenceId(refId);
					// Match an existing link record or create a new record
					if (refList.containsKey(recId)) {
						Link reference = refList.get(recId);
						++duplicate;
						if (diag) {
							log.debug("Matched [{}] [{}] [{}]", reference.getRecordTag(), link.getReferenceId(), reference.getReferenceId());
						}
						reference.setReferenceId(link.getReferenceId());
					} else {
						++created;
						if (diag) {
							log.debug("Created [{}] [{}]", link.getRecordTag(), link.getReferenceId());
						}
						refList.put(recId, link);
					}
					
				}		

			}
		} catch (IOException e) {
			log.info("Error Reading the Reference File: {}", e.getMessage());
			result = false;
		}

		log.info("Reference Set Records Read[{}] - Filtered[{}] - Errors[{}] : Links[{}] = Duplicates[{}] + Created[{}]",
				input, filtered, errors, links, duplicate, created);
		
		return result;
	}
	
	/*
	 * Read the Link set file 
	 */
	private boolean readLinkSet(String fileName, int errorLimit, Map<String, Link> refList) {
		if (StringUtils.isBlank(fileName)) {
			log.error("No Link file specified");
			return false;
		}
		log.info("Link File: {}",fileName);
		try (Reader reader = new FileReader(new File(fileName))) {
			 return parseLinkSet(reader, errorLimit, refList);
		} catch (FileNotFoundException e) {
			log.error("Link file {} not found", fileName);
			return false;
		} catch (Exception e) {
			log.error("Could not read reference file '{}'", fileName, e);
			return false;
		}
	}
	
	/*
	 * Read the Link file This is read second and tries to match the previously read
	 * reference records. If no match is found, a new record can be created
	 *
	 * This is identical logic to ReadTruthSet except for the constructor used and
	 * the fields populated
	 * 
	 * Parameters: FileName is the path and name of the index file The file format
	 * is the OYSTER .link output file containing RefID (Source.RecordID) OysterID
	 * Rule
	 * 
	 * CreateUnmatched: whether to create a new record if no match is found in
	 * LinkResults
	 *
	 */
	private boolean parseLinkSet(Reader reader, int errorLimit, Map<String, Link> refList) {
		boolean diag = config.isDiagnosticMode("link");
		boolean result = true;
		FieldDelimiter delim = config.getLinkDelimiter();
		if (FieldDelimiter.NONE.equals(delim)) {
			log.error("Link delimiter is invalid, processing terminated");
			return false;
		}
		FieldDelimiter idDelim = config.getLinkIdDelimiter();
		int[] recordCols = config.getLinkRecordColumns();
		int clusterCol = config.getLinkClusterColumn(); 
		int ruleCol = config.getLinkRuleColumn(); 
		log.info("Link record.columns:{} cluster.column:{} rule.column:{}", recordCols, clusterCol, ruleCol);
		int filterCol = config.getLinkFilterColumn();
		String filterValue = null;
		if (filterCol > 0) {
			filterValue = config.getLinkFilterValue();
			log.info("Link filtering on value:'{}' in column:{}",filterValue,filterCol);
		}
		int minTokens = config.getMaxLinkColumn();
		int input = 0;
		int filtered = 0;
		int errors = 0;
		int links = 0;
		int matched = 0;
		int created = 0;
		Map<String,List<String>> tuples = new HashMap<String,List<String>>();

		try (BufferedReader in = new BufferedReader(reader)) {
			String s;
			// Get the header line of present
			if (config.isLinkHeader()) {
				s = in.readLine();
				log.info("\tHeader:[{}]", s);
			}
			// Read all the data lines
			while ((s = in.readLine()) != null) {
				// Blank line?
				if (s.trim().length() == 0) {
					continue;
				}
				// Count the input records
				++input;
				// Split the line on the specified delimiter
				String[] tokens = s.split(delim.getValue(),-1);
				// Display the first record for reference
				if (input == 1) {
					log.info("\tRecord #1:{}", String.join(":", tokens));
				}
				if (tokens.length < minTokens) {
					++errors;
					log.info("Invalid Link record [{}] expected {} tokens found {}", s, minTokens, tokens.length);
					// In the first few records, abort
					if (errors > errorLimit) {
						log.info("Aborting ...");
						return false;
					}
					// No further process of this record possible
					continue;
				}
				if (!filterMatch(tokens, filterValue, filterCol)) {
					++filtered;
					continue;
				}

				/**
				 * Modes if ClusterID is supplied
				 * 	1. Single Record ID column with linkID column
				 * 		Match the record ID against existing record ID from truth set
				 * 			IF matched, store linkID and RuleID
				 * 			If not matched, create new reference without reference ID (FP)
				 *  2. Multiple Record ID column with linkID column
				 * 		Match each record ID sequentially against existing record ID from truth set
				 * 			IF matched, store linkID and RuleID
				 * 			If not matched, create new reference without reference ID (FP)
				 *  3, Multiple Record ID column without linkID column (requires transitive closure)
				 *  	Match any record ID with existing record ID
				 *  		If any matched they must all have the same pseudo link ID
				 *  			Use the linkID from those references to create any missing references
				 *  		If not matched, use the next pseudoL lnkId to create the references
				 *  
				 *  Duplicates references are ignored
				 */
				if (clusterCol > 0) {
					/*
					 * Process recordIDs sequentially if we have linkIDs
					 */
					for (int recordCol : recordCols) {
					// Parse the input record
						Link link = new Link();
						++links;
						link.setRecordId(tokens, recordCol, idDelim);
						link.setLinkId(tokens,clusterCol);
						link.setRules(tokens,ruleCol);
						String recId = link.getRecordTag();
						// Match an existing link record or create a new record
						if (refList.containsKey(recId)) {
							Link reference = refList.get(recId);
							++matched;
							if (diag) {
								log.debug("Matched [{}] [{}] [{}]", reference.getRecordTag(), link.getRecordTag(), reference.getReferenceId());
							}
							reference.setLinkId(link.getLinkId());
							reference.setRules(link.getRules());
						} else {
							++created;
							if (diag) {
								log.debug("Created [{}] [{}] [{}]", link.getRecordTag(), link.getLinkId(), link.getRules());
							}
							refList.put(link.getRecordTag(), link);
						}
					}
				} else {
					/*
					 *  Process recordIDs as a group to create tuples for transitive closure
					 *  This creates a tuples map which is used to close all the pairs 
					 *  into the refList
					 */
					String [] pairIds = new String[recordCols.length];
					// Extract all the tuples
					RecordId id = new RecordId();
					for (int i = 0; i < recordCols.length; i++) {
						pairIds[i] =  id.setRecordId(tokens,recordCols[i],idDelim).getRecordTag();
					}
					// Save them in the tuple map for transitive closure
					savePairs(tuples,pairIds,diag);
				}
			}
			
			/*
			 *  If we processed pairs then we need to perform transitive closure on the tuples map
			 *  to create the closure map and then add all those entries to the refList
			 *  
			 *  NOTE: there are no rules tracked from the pairs
			 */
			if ((clusterCol == 0) && tuples.size() > 0) {
				// Perform transitive closure on all the pairs
				Map<String, String> closure = closePairs(tuples,diag);
				for (Map.Entry<String, String> entry : closure.entrySet()) {
					// In this list the recordID is only the recordTag
					String recId = entry.getKey();
					String linkId = entry.getValue();
					Link link = new Link();
					++links;
					link.setRecordId(recId);
					link.setLinkId(linkId);
					// Match an existing link record or create a new record
					if (refList.containsKey(recId)) {
						Link reference = refList.get(recId);
						++matched;
						if (diag) {
							log.debug("Matched [{}] [{}] [{}]", reference.getRecordTag(), link.getRecordTag(), reference.getReferenceId());
						}
						reference.setLinkId(link.getLinkId());
					} else {
						++created;
						if (diag) {
							log.debug("Created [{}] [{}] [{}]", link.getRecordTag(), link.getLinkId(), link.getRules());
						}
						refList.put(recId, link);
					}
					
				}		
			}
		} catch (IOException e) {
			log.info("Error Reading the Link File: {}", e.getMessage());
			result = false;
		}
		
		log.info("Link Set Records Read[{}] - Filtered[{}] - Errors[{}] : Links[{}] = Matched[{}] + Created[{}]", 
				input, filtered, errors, links, matched, created);
		return result;
	}
	
	/**
	 * Build the transitive closure list for input pairs
	 * Each key refId should appear in at least two lists attached to itself and its pair
	 * TODO Is this practical with large lists of pairs as storage may be an issue?
	 * TODO Is it enough to save reference to the right or should each key have all partners?
	 * 
	 * @param closure map of refID to a list of RefIDs it is paired with
	 * @param refIDs to add to the map
	 */
	protected void savePairs(Map<String,List<String>> tuples, String [] refIds, boolean diag) {
		if ((tuples == null) || refIds == null)
			return;
		// Make sure we have a pair
		if (refIds.length < 2)
			return;
		if (diag)
			log.debug("Pair IDs: {}", refIds.toString());
		// Process each left-right pair (ie. for A,B,C then A-B, A-C, B-C)
		for (int i = 0; i < refIds.length-1; i++) {
			// Get the left element
			String left = refIds[i];
			// Don't process null or blank refIds
			if (StringUtils.isBlank(left))
				continue;
			List<String> others;
			if (tuples.containsKey(left)) {
				// Already know this refID
				others = tuples.get(left);
			} else {
				// Otherwise add it and its empty list
				others = new ArrayList<String>();
				tuples.put(left, others);
			}
			for (int j = i+1; j < refIds.length; j++) {
				// Get each possible right element;
				String right = refIds[j];
				// Don't process null or blank refIds
				if (StringUtils.isBlank(right))
					continue;
				// Already in the list
				if (others.contains(right))
					continue;
				others.add(right);
			}
			if (diag)
				log.debug("Pairs: {} -> {}", left,others);
		}
	}
	
	/**
	 * Perform transitive closure on the tuples and return
	 * a map of refID to clusterID
	 * 
	 * @param tuples to process
	 * @return Map containing one entry for each refID and its pseudoClusterId;
	 */
	protected Map<String, String> closePairs(Map<String,List<String>> tuples, boolean diag) {
		Map<String, String> closure = new HashMap<String, String>();
		long pseudoId = 0L;
		log.info("Performing transitive closure on {} tuples",tuples.size());
		for (String refId : tuples.keySet()) {
			// Get or create the id for this refId
			String id = null;
			if (closure.containsKey(refId)) {
				// If this refID already has a cluster ID use it
				id = closure.get(refId);
			} else {
				// If any related ref already has a cluster ID use it
				for (String pairId : tuples.get(refId)) {
					id = closure.get(pairId);
					if (id != null)
						break;
				}
				// If we still don't have a cluster ID generate one
				if (id == null)
					id = String.format("%08d", ++pseudoId);
				closure.put(refId, id);
				if (diag)
					log.debug("Closed: {} -> {}", refId, id);

			}
			for (String pairId : tuples.get(refId)) {
				closure.put(pairId, id);
				if (diag)
					log.info("Closed: {} -> {}", refId, id);
		}
		}
		return closure;
	}
	
	/**
	 * If there is a filter for this set, see if the value matches
	 * @param tokens tokens from the record under test
	 * @param value to test for
	 * @param column number relative to 1
	 * @return
	 */
	protected boolean filterMatch(String[] tokens, String value, int column) {
		// No filter specified
		if (column <= 0)
			return true;
		if (StringUtils.isBlank(value))
			return true;
		if (tokens == null)
			return true;
		// If the value matches the specified token return true
		if (value.equals(tokens[column-1]))
			return true;
		return false;
	}
	
	/**
	 * Perform outer or inner join of the two link sets
	 * 
	 * @param refList - list to join on recordID
	 * @param outerJoin - true=outer join, false=inner join
	 * @return new joined list
	 */
	private List<Link> join(Map<String, Link> refList, boolean outerJoin) {
		int input = 0;
		int noLinkId = 0;
		int noReferenceId = 0;
		int joined = 0;
		
		log.info("Performing {} join for {} references", outerJoin ? "OUTER" : "INNER", refList.size());
		// Create a new empty list sized to the original
		List<Link> newList = new ArrayList<Link>(refList.size());
		for (Map.Entry<String, Link> entry : refList.entrySet()) {
			input++;
			Link link = entry.getValue();
			// Inner joins must have both linkId and truthId
			if (StringUtils.isBlank(link.getLinkId())) {
				noLinkId++;
				if (!outerJoin) {
					log.info("No Matching LinkID: {}",link);
					continue;
				}
			}
			if (StringUtils.isBlank(link.getReferenceId())) {
				noReferenceId++;
				if (!outerJoin) {
					log.info("No Matching RefID: {}",link);
					continue;
				}
			}
			joined++;
			newList.add(link);
		}
		// Clear the original list to free up memory
		refList.clear();
		
		log.info("Joined Link Set Records Total[{}]  NoLinkId[{}]  NoReferenceId[{}]  Joined[{}]", 
				new Object[] { input, noLinkId, noReferenceId, joined });
		return newList;
	}
}