package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.Link
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents the results of linking
 * There is one record for each reference
 * 
 * Properties are:
 *  - Source from the link set (Source.RecordID)
 *  - Record ID from the link set (Source.RecordID)
 *  - Link ID from the the link set
 *  - Matching Rule from the  link set
 *  - Reference ID from the reference set
 *  
 *  @author James true
 */
public class Link extends RecordId implements Comparable<Link> {

	private static final Logger log = LoggerFactory.getLogger(Link.class);

	/*
	 * Cluster ID from record linking 
	 */
	private String linkId;
	
	/*
	 * Cluster ID from truth/reference 
	 */
	private String referenceId;
	
	/*
	 * Rules used to link, this is primarily for Oyster
	 */
	private String rules;

	/**
	 * Link (Oyster) ID assigned by the ER engine
	 */
	public String getLinkId() {
		return this.linkId;
	}
	public Link setLinkId(String value) {
		this.linkId = clean(value);
		return this;
	}
	public Link setLinkId(String [] tokens, int col) {
		String token = extractToken(tokens, col);
		setLinkId(token);
		if (token == null) {
			log.error("LinkID column {} out of range, token count {}", col, tokens.length);
		}
		return this;
	}

	/**
	 * Id from the reference set
	 */
	public String getReferenceId() {
		return this.referenceId;
	}
	public Link setReferenceId(String value) {
		this.referenceId = clean(value);
		return this;
	}
	public Link setReferenceId(String [] tokens, int col) {
		String token = extractToken(tokens, col);
		setReferenceId(token);
		if (token == null) {
			log.error("ReferenceID col {} out of range, token count {}", col, tokens.length);
		}
		return this;
	}
	
	/**
	 * The constructed Agreement string
	 * 	"linkId.referenceId"
	 * @return
	 */
	public String getAgreement() {
		return this.linkId + "." + this.referenceId;
	}

	/**
	 * Oyster rules uses to link the reference
	 */
	public String getRules() {
		return this.rules;
	}
	public Link setRules(String value) {
		this.rules = clean(value);
		return this;
	}
	public Link setRules(String [] tokens, int col) {
		String token = extractToken(tokens, col);
		setRules(token);
		if (token == null) {
			log.error("Rules column {} out of range, token count {}", col, tokens.length);
		}
		return this;
	}

	/** 
	 * Null argument constructor
	 */
	public Link() {
		super();
		setLinkId(null);
		setRules(null);
		setReferenceId(null);
	}

	/**
	 * Constructor taking RecID
	 */
	public Link(String recId) {
		super(recId);
		setLinkId(null);
		setRules(null);
		setReferenceId(null);
		log.debug("Created Link - {}", this.toString());
	}

	/**
	 * Constructor taking RecID, OysterID, Rules from Link file
	 */
	public Link(String recId, String linkId, String rules, String referenceId) {
		super(recId);
		setLinkId(linkId);
		setRules(rules);
		setReferenceId(referenceId);
		log.debug("Created Link - {}", this.toString());
	}

	/**
	 * Get the CSV header line 
	 * @return
	 */
	public static String getLinkHeader() {
		return "RecordID,LinkId,Rules,ReferenceId\n";
	}
	
	/**
	 * Return this object in CSV format
	 * 
	 * @return CSV line string
	 */
	public String getLinkAsCSV() {
		StringBuilder sb = new StringBuilder();
		sb.append("\"").append(getRecordId()).append("\"").append(",");
		sb.append("\"").append(getLinkId()).append("\"").append(",");
		sb.append("\"").append(getRules()).append("\"").append(",");
		sb.append("\"").append(getReferenceId()).append("\"").append("\n");
		return sb.toString();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("RecID[").append(getRecordId()).append("] ");
		sb.append("LinkID[").append(getLinkId()).append("] ");
		sb.append("Rules[").append(getRules()).append("] ");
		sb.append("ReferenceID[").append(getReferenceId()).append("]");
		return sb.toString();
	}
	
	/**
	 * Comparable method to implement the natural sort order 
	 * for this type of object by comparing the linkId
	 * This will sort on equivalent links
	 */
	@Override
	public int compareTo(Link o) {
		return this.getLinkId().compareTo(o.getLinkId());
	}
	
	/**
	 * Comparator on LinkId
	 * 
	 * @param r1 Link 1
	 * @param r2 Link 2
	 * @return -1|0|+1
	 */
	public static int compareLinkId(Link r1, Link r2) {
		
		return r1.getLinkId().compareTo(r2.getLinkId());
	}

	/**
	 * Comparator on ReferenceId
	 * 
	 * @param r1 Link 1
	 * @param r2 Link 2
	 * @return -1|0|+1
	 */
	public static int compareReferenceId(Link r1, Link r2) {
		
		return r1.getReferenceId().compareTo(r2.getReferenceId());
	}
	
	/**
	 * Comparator on Agreement (linkId.referenceId)
	 * 
	 * @param r1 Link 1
	 * @param r2 Link 2
	 * @return -1|0|+1
	 */
	public static int compareAgreement(Link r1, Link r2) {
		return r1.getAgreement().compareTo(r2.getAgreement());
	}

}
