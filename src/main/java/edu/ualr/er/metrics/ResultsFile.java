package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.ResultFile
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents the results output file created based on the 
 * report.results.filename= configuration parameter
 * 
 * @author James True
 *
 */
public class ResultsFile {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	
	protected String filename;
	protected boolean append;
	protected boolean exists;
	protected BufferedWriter writer;
	protected boolean status;
	
	public ResultsFile(String filename, boolean append) {
		this.status = false;
		if (StringUtils.isBlank(filename)) {
			this.filename = null;
			log.error("No FileName specified");
			return;
		}
		this.filename = filename;
		this.append = append;
		try {
			File file = new File(this.filename);
			// Remember if this file already exists
			this.exists = file.exists();
			this.writer = new BufferedWriter(new FileWriter(file,append));
			this.status = true;
		} catch (IOException e) {
			log.error("Error Opening file: {}", this.filename);
		}
	}
	
	/**
	 * Is the output file open and ok
	 * @return true=opened, false=closed or error
	 */
	public boolean isStatusOk() {
		return this.status;
	}
	
	protected void setStatus(boolean status) {
		this.status = status;
	}
	
	/**
	 * Close the writer
	 * 
	 * This is required to prevent memory leaks. 
	 */
	public void close() {
		if (this.writer != null) {
			try {
				this.writer.close();
			} catch (IOException e) {
				log.error("Error closing file: {}", this.filename);
			}
		}
		this.status = false;
	}

	/**
	 * Write a line to the file
	 * 
	 * @param Metric Values to be written
	 */
	public void writeResults(ERMetricsValues values, String reference) {
		if (values == null) {
			log.error("No result values to write");
			return;
		}
		try {
			// If the file is available
			if (status) {
				// If the file didn't exists, write the header line
				if (append && !exists) {
					this.writer.write(getHeader());
				}
				// Append the results to the file
				this.writer.write(getResults(values, reference));
			}
		} catch (IOException e) {
			// report the error and close the file
			log.info("Error Writing the results to File: {}", e.getMessage());
			close();
		}
	}
	/**
	 * return the CSV header line for the pair type
	 * 
	 * @return header line
	 */
	protected String getHeader() {
		return "# ERMetrics Results\n";
	}
	
	/**
	 * Return the CSV line for the pair type
	 * 
	 * @return detail line
	 */
	protected String getResults(ERMetricsValues values, String reference) {
		StringBuilder sb = new StringBuilder();
		sb.append("Date=").append(new SimpleDateFormat("yyyy-MM-dd").format(new Date())).append("\n");
		sb.append("Time=").append(new SimpleDateFormat("HH:mm:ss").format(new Date())).append("\n");
		sb.append("Reference=").append(StringUtils.wrapIfMissing(reference,'"')).append("\n");
		sb.append("Recs=").append(values.getRecs()).append("\n");
		sb.append("EC=").append(values.getEC()).append("\n");
		sb.append("LC=").append(values.getLC()).append("\n");
		sb.append("AC=").append(values.getAC()).append("\n");
		sb.append("D=").append(values.getD()).append("\n");
		sb.append("E=").append(values.getE()).append("\n");
		sb.append("L=").append(values.getL()).append("\n");
		sb.append("A=").append(values.getA()).append("\n");
		sb.append("TP=").append(values.getTP()).append("\n");
		sb.append("FP=").append(values.getFP()).append("\n");
		sb.append("TN=").append(values.getTN()).append("\n");
		sb.append("FN=").append(values.getFN()).append("\n");
		sb.append("FPRate=").append(values.getFPR()).append("\n");
		sb.append("FNRate=").append(values.getFNR()).append("\n");
		sb.append("Accuracy=").append(values.getAccuracy()).append("\n");
		sb.append("Precision=").append(values.getPrecision()).append("\n");
		sb.append("Recall=").append(values.getRecall()).append("\n");
		sb.append("F-Measure=").append(values.getFMeasure()).append("\n");
		sb.append("TWi=").append(values.getTwi()).append("\n");
		return sb.toString();
	}
}
