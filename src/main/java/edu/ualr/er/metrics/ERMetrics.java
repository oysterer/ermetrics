package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.ERMetrics
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;

/**
 * This is the main class for the ERMetrics program
 * 
 * @author James True
 *
 */

public class ERMetrics {
	
	public static Logger ErLog;
	public static Map<String,String> options = new HashMap<String,String>(4);
	
	public static final String OPTION_CONFIG_FILE = "config";
	public static final String OPTION_ENCRYPT = "encrypt";
	public static final String OPTION_TITLE = "title";
	public static final String OPTION_LOG_FILE = "log";
	public static final String OPTION_LOG_LEVEL = "level";
	public static final String OPTION_QUIET = "quiet";
	
	public static ERMetricsValues values;

	/**
	 * Entry point of the application. This will try to: Get a properties file name
	 * from the command line Load the properties (key=value) Run the report
	 * <p>
	 * The logger isn't created until just before running the report so output
	 * is kept to a minimum during option parsing, only errors are written out.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		ERMetricsReport report = new ERMetricsReport();

		// Get the package
        Package pkg = edu.ualr.er.metrics.ERMetrics.class.getPackage();
        // Get the version from the manifest
		report.setVersion(pkg.getImplementationVersion());

		values = report.getValues();
		// Default file name, can be changed from the command line
		options.put(OPTION_CONFIG_FILE, "ERMetrics.properties");
		String option = null;
		for (String arg : args) {
			if (StringUtils.startsWith(arg, "--")) {
				option = StringUtils.substring(arg, 2).toLowerCase();
				if (StringUtils.isBlank(option))
					continue;
				String [] tokens = option.split("[=:]");
				if (tokens.length >= 2) {
					// Save the option with the value
					options.put(tokens[0],tokens[1]);
				} else {
					// Save the option with an empty value
					options.put(option, "");
				}
			} else {
				// By itself, assume it is the configuration file name
				options.put(OPTION_CONFIG_FILE, arg);
			}
		}
		for (Map.Entry<String, String> entry: options.entrySet()) {
			String key = entry.getKey();
			// The configuration file name
			if (OPTION_CONFIG_FILE.equals(key)) {
				report.setConfigFileName(entry.getValue());
				continue;
			}
			// Save the truth set as an encrypted file (no associated value)
			if (OPTION_ENCRYPT.equals(key)) {
				report.setEncryptTruth(true);
				continue;
			}
			// Title the run
			if (OPTION_TITLE.equals(key)) {
				report.setTitle(entry.getValue());
				continue;
			}
			// Log filename option
			if (OPTION_LOG_FILE.equals(key)) {
				continue;
			}
			// Log level option
			if (OPTION_LOG_LEVEL.equals(key)) {
				continue;
			}
			// Shortcut for level=ERROR
			if (OPTION_QUIET.equals(key)) {
				options.put(OPTION_LOG_LEVEL,"ERROR");
				continue;
			}
			// Invalid parameter terminates the program (no logging in this module)
			System.out.println("Unknown option: " + key);
			return;
		}

		/*
		 * LOG_OPTION = NULL	use standard Rolling ERMetrics.log
		 * LOG_OPTION = "" 		use date formatted unique filename
		 * LOG_OPTION = string  use this as the log file name
		 */
		ErLog = CustomLogger.createLoggerFor("edu.ualr",options.get(OPTION_LOG_FILE),options.get(OPTION_LOG_LEVEL));
		report.run();
	}
}