package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.PairFileFN
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * False Negative pair file 
 * 
 * @author James True
 *
 */
public class PairFileFN extends PairFile {

	public PairFileFN(String filename, long limit) {
		super(filename, limit);
	}
	
	@Override
	protected String getPairType() {
		return "False Negative";
	}
	
	@Override
	protected String getPairHeader() {
		return "LeftRecID,LeftLinkID,LeftRules,RightRecID,RightLinkID,RightRules,TruthID\n";
	}

	@Override
	protected String getPairAsCsv(Pair pair) {
		StringBuilder sb = new StringBuilder();
		Link leftLink = pair.getLeftLink();
		if (leftLink != null) {
			sb.append("\"").append(leftLink.getRecordId()).append("\"").append(",");
			sb.append("\"").append(leftLink.getLinkId()).append("\"").append(",");
			sb.append("\"").append(leftLink.getRules()).append("\"").append(",");
		} else {
			sb.append(",,,");
		}
		Link rightLink = pair.getRightLink();
		if (rightLink != null) {
			sb.append("\"").append(rightLink.getRecordId()).append("\"").append(",");
			sb.append("\"").append(rightLink.getLinkId()).append("\"").append(",");
			sb.append("\"").append(rightLink.getRules()).append("\"").append(",");
			sb.append("\"").append(rightLink.getReferenceId()).append("\"").append("\n");
		} else {
			sb.append(",,,\n");
		}
		return sb.toString();
	}

	@Override
	protected String getKey(Link ref) {
		return ref.getReferenceId();
	}

	@Override
	protected boolean compareLinks(Link left, Link right) {
		return !left.getLinkId().equals(right.getLinkId());
	}
}
