package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.ERMetricsValues
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


/**
 * Holds the Link counts for the different conditions
 * and returns formatted values for reporting
 * 
 * @author James True
 *
 */
public class ERMetricsValues {

	// Output format
	// https://docs.oracle.com/javase/7/docs/api/java/util/Formatter.html
	private static final String NUMBER_FORMAT = "%,d";
	private static final String DOUBLE_FORMAT = "%.8f";

	private ReferenceCount total; // Recs & D
	private ReferenceCount reference; // E & EC
	private ReferenceCount linked; // L & LC
	private ReferenceCount associated; // TP & AC
	private Long fpPairCount;
	private Long fnPairCount;

	public ERMetricsValues() {
		total = new ReferenceCount();
		reference = new ReferenceCount();
		linked = new ReferenceCount();
		associated = new ReferenceCount();
		fpPairCount = 0L;
		fnPairCount = 0L;
	}

	/*
	 * Total count values
	 */

	public ReferenceCount getTotal() {
		return this.total;
	}

	public String getRecs() {
		return String.format(NUMBER_FORMAT, total.getCount());
	}

	public String getD() {
		return String.format(NUMBER_FORMAT, total.getPairs());
	}

	/*
	 * Equivalent count values
	 */
	public ReferenceCount getReference() {
		return this.reference;
	}

	public String getE() {
		return String.format(NUMBER_FORMAT, reference.getPairs());
	}

	public String getEC() {
		return String.format(NUMBER_FORMAT, reference.getClusters());
	}

	/*
	 * Linked count values
	 */
	public ReferenceCount getLinked() {
		return this.linked;
	}

	public String getL() {
		return String.format(NUMBER_FORMAT, linked.getPairs());
	}

	public String getLC() {
		return String.format(NUMBER_FORMAT, linked.getClusters());
	}

	/*
	 * Associated count values True Positive pairs Associated clusters
	 */
	public ReferenceCount getAssociated() {
		return this.associated;
	}
	
	public String getA() {
		return String.format(NUMBER_FORMAT, associated.getPairs());
	}

	public String getAC() {
		return String.format(NUMBER_FORMAT, associated.getClusters());
	}
	
	// True Positive is the associated pair count
	public String getTP() {
		return getA();
	}

	/*
	 * Calculated count values
	 */

	// False Positive
	public long fp() {
		return linked.getPairs() - associated.getPairs();
	}

	public String getFP() {
		return String.format(NUMBER_FORMAT, fp());
	}

	public String getFpCount() {
		return String.format(NUMBER_FORMAT, fpPairCount);
	}

	// True Negative
	public long tn() {
		return total.getPairs() - associated.getPairs() - fp() - fn();
	}

	public String getTN() {
		return String.format(NUMBER_FORMAT, tn());
	}

	// False Negative
	public long fn() {
		return reference.getPairs() - associated.getPairs();
	}

	public String getFN() {
		return String.format(NUMBER_FORMAT, fn());
	}

	public String getFnCount() {
		return String.format(NUMBER_FORMAT, fnPairCount);
	}

	/*
	 * Calculated ratio values (0.0 - 1.0)
	 */

	public double fpr() {
		return (double) fp() / (double) (tn() + (double) fp());
	}

	public String getFPR() {
		return String.format(DOUBLE_FORMAT, fpr());
	}

	public double fnr() {
		return (double) fn() / (double) reference.getPairs();
	}

	public String getFNR() {
		return String.format(DOUBLE_FORMAT, fnr());
	}

	public double accuracy() {
		return ((double) associated.getPairs() + (double) tn()) / (double) total.getPairs();
	}

	public String getAccuracy() {
		return String.format(DOUBLE_FORMAT, accuracy());
	}

	public double precision() {
		return (double) associated.getPairs() / ((double) associated.getPairs() + (double) fp());
	}

	public String getPrecision() {
		return String.format(DOUBLE_FORMAT, precision());
	}

	public double recall() {
		return (double) associated.getPairs() / ((double) associated.getPairs() + (double) fn());
	}

	public String getRecall() {
		return String.format(DOUBLE_FORMAT, recall());
	}

	public double fMeasure() {
		double precision = precision();
		double recall = recall();
		return (2 * (precision * recall) / (precision + recall));
	}

	public String getFMeasure() {
		return String.format(DOUBLE_FORMAT, fMeasure());
	}

	public double twi() {
		return Math.sqrt((double) linked.getClusters() * (double) reference.getClusters())
				/ (double) associated.getClusters();

	}

	public String getTwi() {
		return String.format(DOUBLE_FORMAT, twi());
	}
}
