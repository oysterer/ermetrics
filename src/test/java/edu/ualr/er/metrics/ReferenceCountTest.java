package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.ReferenceCountTest
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ReferenceCountTest {

	@Test
	public void testConstructor() {
		ReferenceCount counts = new ReferenceCount();
		assertNotNull(counts);
		assertEquals(counts.getCount(),0);
		assertEquals(counts.getPairs(),0);
		assertEquals(counts.getClusters(),0);
	}

	@Test
	public void testSetAddGet() {
		ReferenceCount counts = new ReferenceCount();

		counts.setCount(1);
		assertEquals(counts.getCount(),1);
		counts.addCount(1);
		assertEquals(counts.getCount(),2);

		counts.setPairs(1);
		assertEquals(counts.getPairs(),1);
		counts.addPairs(1);
		assertEquals(counts.getPairs(),2);
		
		counts.setClusters(1);
		assertEquals(counts.getClusters(),1);
		counts.addClusters(1);
		assertEquals(counts.getClusters(),2);
	}

	@Test
	public void testComputePairsStatic() {
		long pairs = ReferenceCount.computePairs(4);
		assertEquals(6,pairs);
	}

	@Test
	public void testcomputePairs() {
		ReferenceCount counts = new ReferenceCount();

		// 1 or less = 0 pairs
		counts.setCount(1);
		counts.computePairs();
		assertEquals(0,counts.getPairs());
		
		// N * N-1 / 2
		counts.setCount(5);
		counts.computePairs();
		assertEquals(10,counts.getPairs());
	}
	

	@Test
	public void testComputeClusters() {
		ReferenceCount counts = new ReferenceCount();
		
		List<Link> list = new ArrayList<Link>(20);
		// Cluster 1, 6 pairs
		list.add(new Link("01","AAA","",""));
		list.add(new Link("01","AAA","",""));
		list.add(new Link("01","AAA","",""));
		list.add(new Link("01","AAA","",""));
		// Cluster 2, 1 pair
		list.add(new Link("01","BBB","",""));
		list.add(new Link("01","BBB","",""));
		// Cluster 3, 3 pairs
		list.add(new Link("01","CCC","",""));
		list.add(new Link("01","CCC","",""));
		list.add(new Link("01","CCC","",""));
		// Cluster 4, 15 pairs
		list.add(new Link("01","DDD","",""));
		list.add(new Link("01","DDD","",""));
		list.add(new Link("01","DDD","",""));
		list.add(new Link("01","DDD","",""));
		list.add(new Link("01","DDD","",""));
		list.add(new Link("01","DDD","",""));
		// Cluster 5, 10 pairs
		list.add(new Link("01","EEE","",""));
		list.add(new Link("01","EEE","",""));
		list.add(new Link("01","EEE","",""));
		list.add(new Link("01","EEE","",""));
		list.add(new Link("01","EEE","",""));
		// 20 Records, 35 Pairs, 5 Clusters
		long count = counts.computeClusters(list, Link::getLinkId);
		assertEquals(20,count);
		assertEquals(20,counts.getCount());
		assertEquals(35,counts.getPairs());
		assertEquals(5,counts.getClusters());
	}

}
