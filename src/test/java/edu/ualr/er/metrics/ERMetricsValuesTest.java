package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.ERMetricsValuesTest
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ERMetricsValuesTest {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	private void setValues(ERMetricsValues values) {
		values.getTotal().setCount(617);
		values.getTotal().computePairs();
		values.getReference().setPairs(4494);
		values.getReference().setClusters(50);
		values.getLinked().setPairs(6993);
		values.getLinked().setClusters(45);
		values.getAssociated().setPairs(4300);
		values.getAssociated().setClusters(62);
	}

	@Test
	public void testInitializedValues() {
		ERMetricsValues values = new ERMetricsValues();
		assertNotNull(values);
		assertNotNull(values.getTotal());
		assertNotNull(values.getReference());
		assertNotNull(values.getLinked());
		assertNotNull(values.getAssociated());
		
		setValues(values);
		assertEquals(617,values.getTotal().getCount());
		assertEquals(190036,values.getTotal().getPairs());
		assertEquals(4494,values.getReference().getPairs());
		assertEquals(50,values.getReference().getClusters());
		assertEquals(6993,values.getLinked().getPairs());
		assertEquals(45,values.getLinked().getClusters());
		assertEquals(4300,values.getAssociated().getPairs());
		assertEquals(62,values.getAssociated().getClusters());
	}

	@Test
	public void testComputedValues() {
		ERMetricsValues values = new ERMetricsValues();
		assertNotNull(values);
		
		// Set the count values
		setValues(values);
		assertEquals(190036,values.getTotal().getPairs());

		// Assert the count calculations
		assertEquals(2693,values.fp());
		assertEquals(182849,values.tn());
		assertEquals(194,values.fn());
		
		// Assert the rate calculations
		assertEquals(0.01451423,values.fpr(),0.0000001);
		assertEquals(0.04316867,values.fnr(),0.0000001);
		assertEquals(0.98480814,values.accuracy(),0.0000001);
		assertEquals(0.61490061,values.precision(),0.0000001);
		assertEquals(0.95683133,values.recall(),0.0000001);
		assertEquals(0.74867241,values.fMeasure(),0.0000001);
		assertEquals(0.76506718,values.twi(),0.0000001);
		
	}

	@Test
	public void testOutput() {
		ERMetricsValues values = new ERMetricsValues();
		assertNotNull(values);
		setValues(values);
		
		log.info("Total Distinct Records ID values (N) = {}",values.getRecs());
		log.info("Total Pairs (D) = ((N * N-1) / 2) = {}",values.getD());
		log.info("Truth Set Equivalent Pairs (E) = {}",values.getE());
		log.info("Truth Set Clusters (EC) = {}",values.getEC());
		log.info("Link Set Linked Pairs (L) = {}",values.getL());
		log.info("Link Set Clusters (LC) = {}",values.getLC());
		log.info("True Positive Pairs (TP) = {}",values.getTP());
		log.info("Truth+Link Set Agreement Clusters (AC) = {}",values.getAC());
		log.info("False Positive Pairs (FP) = L -TP = {}",values.getFP());
		log.info("True Negative Pairs (TN) = TotalPairs - TP - FP - FN = {}",values.getTN());
		log.info("False Negative Pairs (FN) = E - TP = {}",values.getFN());
		log.info("False Positive Rate = FP / (TN + FP) = {}",values.getFPR());
		log.info("False Negative Rate = FN / E = {}",values.getFNR());
		log.info("Accuracy = (TP + TN) / TotalPairs = {}",values.getAccuracy());
		log.info("Precision = TP / (TP + FP) = {}",values.getPrecision());
		log.info("Recall = TP / (TP + FN) = {}",values.getRecall());
		log.info("F-Measure = {}",values.getFMeasure());
		log.info("TWi = {}",values.getTwi());
	}
}
