package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.PairFileFPTest
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PairFileFPTest extends AbstractFileTest {
	
	private String recId1 = "FileA.Rec1";
	private String recId2 = "Rec2";
	private String linkId = "LinkID";
	private String truthId = "TruthID";
	private String rules = "R1,@,#";
	private Link ref1 = new Link(recId1,linkId,rules,truthId);
	private Link ref2 = new Link(recId2,"L2","X,Z","T2");
	
	/*
	 * Test Get Pair Type
	 */
	@Test
	public void testGetPairType() {
		PairFileFP pairFile = new PairFileFP(fname,1);
		//System.out.println(pairFile.getPairType());
		assertEquals("False Positive", pairFile.getPairType());
	}

	/*
	 * Test Get Pair Header
	 */
	@Test
	public void testGetPairHeader() {
		PairFileFP pairFile = new PairFileFP(fname,1);
		//System.out.println(pairFile.getPairHeader());
		assertEquals("LeftRecID,LeftTruthID,RightRecID,RightTruthID,LinkID,Rules\n", pairFile.getPairHeader());
	}

	/*
	 * Test Get Pair Header
	 */
	@Test
	public void testGetPairAsCsv() {
		PairFileFP pairFile = new PairFileFP(fname,1);
		//System.out.println(pairFile.getPairAsCsv(new Pair(ref1,ref2)));
		assertEquals("\"FileA.Rec1\",\"TruthID\",\"Rec2\",\"T2\",\"L2\",\"X,Z\"\n", pairFile.getPairAsCsv(new Pair(ref1,ref2)));
	}
}
