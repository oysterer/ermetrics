package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.ERMetricsTest
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

//import static org.junit.Assert.*;

import org.junit.Test;

public class ERMetricsTest {

	@Test
	public void testMain() {
		System.out.println(">>>>>>>>>> testMain >>>>>>>>>>>");
		ERMetrics.main(new String[] {"src/test/resources/ERMetricsTest.properties"});
		
		ERMetricsValues values = ERMetrics.values;
		assertNotNull(values);
		
		// Make sure we are reading the correct file
		assertEquals(190036,values.getTotal().getPairs());

		// Assert the count calculations
		assertEquals(2693,values.fp());
		assertEquals(182849,values.tn());
		assertEquals(194,values.fn());
		
		// Assert the rate calculations
		assertEquals(0.01451423,values.fpr(),0.0000001);
		assertEquals(0.04316867,values.fnr(),0.0000001);
		assertEquals(0.98480814,values.accuracy(),0.0000001);
		assertEquals(0.61490061,values.precision(),0.0000001);
		assertEquals(0.95683133,values.recall(),0.0000001);
		assertEquals(0.74867241,values.fMeasure(),0.0000001);
		assertEquals(0.76506718,values.twi(),0.0000001);
	}

}
