package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.LinkTest
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class LinkTest {
	
	private static String source = "File1";
	private static String recId = "A1234";
	private static String complexRecId = "\"www.ebay.com//12345\"";
	private static String cleanComplexRecId = "www.ebay.com//12345";
	private static String srcRefId = source + "." + recId;
	private static String srcComplexRefId = source + "." + complexRecId;
	private static String linkId = "XYZ123";
	private static String rules = "@";
	private static String truthId = "2";
	
	private static int refCol = 1;
	private static int linkCol = 2;
	private static int rulesCol = 3;
	private static int truthCol = 4;
	
	private static String [] tokens = { recId, linkId, rules, truthId };

	@Test
	public void testNoArgConstructor() {
		Link ref = new Link();
		assertNotNull(ref);
		assertEquals("",ref.getRecordTag());
		assertEquals("",ref.getLinkId());
		assertEquals("",ref.getRules());
		assertEquals("",ref.getReferenceId());
	}

	@Test
	public void testRecIdConstructor() {
		Link ref = new Link(recId);
		assertNotNull(ref);
		assertEquals(recId,ref.getRecordTag());
		assertEquals("",ref.getLinkId());
		assertEquals("",ref.getRules());
		assertEquals("",ref.getReferenceId());
	}

	@Test
	public void testFullConstructor() {
		Link ref = new Link(recId,linkId,rules,truthId);
		assertNotNull(ref);
		assertEquals(recId,ref.getRecordTag());
		assertEquals(linkId,ref.getLinkId());
		assertEquals(rules,ref.getRules());
		assertEquals(truthId,ref.getReferenceId());
	}

	@Test
	public void testSetRefIdString() {
		Link ref = new Link();
		ref.setRecordId(recId);
		assertEquals(recId,ref.getRecordTag());
	}

	@Test
	public void testSetSrcRefIdString() {
		Link ref = new Link();
		ref.setRecordId(srcRefId);
		assertEquals(source,ref.getRecordSource());
		assertEquals(recId,ref.getRecordTag());
		
		ref.setRecordId(srcComplexRefId);
		assertEquals(source,ref.getRecordSource());
		assertEquals(cleanComplexRecId,ref.getRecordTag());
		
		ref.setRecordId(srcComplexRefId,FieldDelimiter.PERIOD);
		assertEquals(source,ref.getRecordSource());
		assertEquals(cleanComplexRecId,ref.getRecordTag());
	}

	@Test
	public void testSetRefIdStringArrayInt() {
		Link ref = new Link();
		ref.setRecordId(tokens,refCol);
		assertEquals(recId,ref.getRecordTag());
	}

	@Test
	public void testSetLinkIdString() {
		Link ref = new Link();
		ref.setLinkId(linkId);
		assertEquals(linkId,ref.getLinkId());
	}

	@Test
	public void testSetLinkIdStringArrayInt() {
		Link ref = new Link();
		ref.setLinkId(tokens,linkCol);
		assertEquals(linkId,ref.getLinkId());
	}

	@Test
	public void testSetRulesString() {
		Link ref = new Link();
		ref.setRules(rules);
		assertEquals(rules,ref.getRules());
	}

	@Test
	public void testSetRulesStringArrayInt() {
		Link ref = new Link();
		ref.setRules(tokens,rulesCol);
		assertEquals(rules,ref.getRules());
	}

	@Test
	public void testSetTruthIdString() {
		Link ref = new Link();
		ref.setReferenceId(truthId);
		assertEquals(truthId,ref.getReferenceId());
	}

	@Test
	public void testSetTruthIdStringArrayInt() {
		Link ref = new Link();
		ref.setReferenceId(tokens,truthCol);
		assertEquals(truthId,ref.getReferenceId());
	}

	@Test
	public void testNegativeCol() {
		Link ref = new Link();
		ref.setReferenceId(tokens,-1);
		assertEquals("",ref.getReferenceId());
	}

	@Test
	public void testOverflowCol() {
		Link ref = new Link();
		ref.setReferenceId(tokens,10);
		assertEquals("",ref.getReferenceId());
	}
	
	@Test
	public void testClean() {
		assertEquals(cleanComplexRecId,Link.clean(complexRecId));
		assertEquals("",Link.clean(""));
		assertEquals("",Link.clean(null));
	}

}
