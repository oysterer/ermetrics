package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.QuiteOutputTest
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;

//import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Test the --quite option to suppress console output
 * @author jatrue
 *
 */
public class QuietOutputTest {

	@Test
	public void testMain() {
		System.out.println(">>>>>>>>>> ERMetrics.main Quiet Output Test >>>>>>>>>>>");
		ERMetrics.main(new String[] {"--quiet", "src/test/resources/Assignment3.properties"});
		assertEquals("ERROR",ERMetrics.options.get(ERMetrics.OPTION_LOG_LEVEL));
		
		ERMetricsValues values = ERMetrics.values;
		assertNotNull(values);
	}

	@Test
	public void ERMetricsReport() {
		System.out.println(">>>>>>>>>> ERMetricsReport Quiet Output Test >>>>>>>>>>>");
		// Set logging to EROR level with rolling appender
		CustomLogger.createLoggerFor("edu.ualr",null,"ERROR");

		// Build the configuration as a stream and load it
		PropertiesConfiguration properties = new PropertiesConfiguration();
		properties.addProperty("title", "Assignment3");
		properties.addProperty("link.filename", "src/test/resources/Assignment3.link");
		properties.addProperty("link.pair.filename", "target/Assignment3.pair.out");
		properties.addProperty("link.heade", "true");
		properties.addProperty("link.delimiter", "comma");
		properties.addProperty("link.record.column", "1");
		properties.addProperty("link.cluster.column", "2");
		properties.addProperty("link.rule.column", "0");
		properties.addProperty("reference.filename", "src/test/resources/Assignment3.Truth.csv");
		properties.addProperty("reference.truth", "true");
		properties.addProperty("reference.header", "true");
		properties.addProperty("reference.delimiter", "comma");
		properties.addProperty("reference.record.column", "1");
		properties.addProperty("reference.cluster.column", "2");
		Configuration config = new Configuration(properties);
		assertEquals("Assignment3",config.getTitle());
		
		// Construct the report class with the config
		ERMetricsReport report = new ERMetricsReport(config);
		
		// Run the report and get the values
		report.run();
		ERMetricsValues values = report.getValues();
		assertNotNull(values);
	}

}
