package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.SavePairsTest
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class SavePairsTest {

	@Test
	public void test() {
		ERMetricsReport report = new ERMetricsReport();
		Map<String,List<String>> tuples = new HashMap<String,List<String>>();
		String [] refIds1 = new String [] {"A", "B", "C", "D"};
		report.savePairs(tuples, refIds1,true);
		for (String key : tuples.keySet()) {
			System.out.println(String.format("%1$s has %2$s", key, tuples.get(key)));
		}
		assertEquals(3,tuples.size());

		String [] refIds2 = new String [] {"C", "E", "F"};
		report.savePairs(tuples, refIds2,true);
		for (String key : tuples.keySet()) {
			System.out.println(String.format("%1$s has %2$s", key, tuples.get(key)));
		}
		assertEquals(4,tuples.size());

		String [] refIds3 = new String [] {"X", "Y", "Z"};
		report.savePairs(tuples, refIds3, true);
		for (String key : tuples.keySet()) {
			System.out.println(String.format("%1$s has %2$s", key, tuples.get(key)));
		}
		assertEquals(6,tuples.size());

		String [] refIds4 = new String [] {"G", "B"};
		report.savePairs(tuples, refIds4,true);
		for (String key : tuples.keySet()) {
			System.out.println(String.format("%1$s has %2$s", key, tuples.get(key)));
		}
		assertEquals(7,tuples.size());
		
		Map<String,String> closure = report.closePairs(tuples,true);
		assertEquals(10,closure.size());
		for (String key : closure.keySet()) {
			System.out.println(String.format("RefID:%1$s ClusterID:%2$s", key, closure.get(key)));
		}
		assertEquals("00000001",closure.get("A"));
		assertEquals("00000001",closure.get("B"));
		assertEquals("00000001",closure.get("C"));
		assertEquals("00000001",closure.get("D"));
		assertEquals("00000001",closure.get("E"));
		assertEquals("00000001",closure.get("F"));
		assertEquals("00000001",closure.get("G"));
		assertEquals("00000002",closure.get("X"));
		assertEquals("00000002",closure.get("Y"));
		assertEquals("00000002",closure.get("Z"));
	}

}
