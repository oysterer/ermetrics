package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.RecordIdTest
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class RecordIdTest {
	
	private static String source = "File1";
	private static String refId = "A1234";
	private static String comploexRefId = "\"www.ebay.com//12345\"";
	private static String cleanComplexRefId = "www.ebay.com//12345";
	private static String recId = source + "." + refId;
	private static String complexRecId = source + "." + comploexRefId;
	
	@Test
	public void testNoArgConstructor() {
		RecordId ref = new RecordId();
		assertNotNull(ref);
		assertEquals("",ref.getRecordId());
		assertEquals("",ref.getRecordSource());
		assertEquals("",ref.getRecordTag());
	}

	@Test
	public void testReferenceConstructor() {
		RecordId ref = new RecordId(refId);
		assertNotNull(ref);
		assertEquals(refId,ref.getRecordId());
		assertEquals(refId,ref.getRecordTag());
		assertEquals("",ref.getRecordSource());
	}

	@Test
	public void testFullConstructor() {
		RecordId ref = new RecordId(recId);
		assertNotNull(ref);
		assertEquals(recId,ref.getRecordId());
		assertEquals(source,ref.getRecordSource());
		assertEquals(refId,ref.getRecordTag());
	}

	@Test
	public void testDelimitedFullConstructor() {
		RecordId ref = new RecordId(recId, FieldDelimiter.PERIOD);
		assertNotNull(ref);
		assertEquals(recId,ref.getRecordId());
		assertEquals(source,ref.getRecordSource());
		assertEquals(refId,ref.getRecordTag());
	}

	@Test
	public void testSetRefIdString() {
		RecordId ref = new RecordId();
		ref.setRecordId(refId, FieldDelimiter.PERIOD);
		assertEquals(refId,ref.getRecordTag());
	}

	@Test
	public void testSetSrcRefIdString() {
		RecordId ref = new RecordId();
		ref.setRecordId(recId,FieldDelimiter.PERIOD);
		assertEquals(source,ref.getRecordSource());
		assertEquals(refId,ref.getRecordTag());
		
		ref.setRecordId(complexRecId);
		assertEquals(source,ref.getRecordSource());
		assertEquals(cleanComplexRefId,ref.getRecordTag());
		
		ref.setRecordId(complexRecId,FieldDelimiter.PERIOD);
		assertEquals(source,ref.getRecordSource());
		assertEquals(cleanComplexRefId,ref.getRecordTag());
	}

	@Test
	public void testClean() {
		assertEquals(cleanComplexRefId,RecordId.clean(comploexRefId));
		assertEquals("",RecordId.clean(""));
		assertEquals("",RecordId.clean(null));
	}

}
