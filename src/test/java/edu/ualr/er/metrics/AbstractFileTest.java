package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.AbstractFileTest
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;

import org.junit.After;

public abstract class AbstractFileTest {
	
	protected static String fname = "src/test/resources/testfile.txt";

	@After
	public void cleanup() {
	    File file = new File(fname); 
	    if (file.delete()) { 
	      // System.out.println("Deleted the file: " + file.getName());
	    } else {
	      // System.out.println("Failed to delete the file: " + file.getName());
	    } 
	}

}
