package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.ConfigurationTest
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ConfigurationTest {
	
	private static String EMPTY_FILE = "empty.properties";
	private static String TEST_FILE = "test.properties";
	private static String CONFIG_FILE = "ERMetrics.properties";

	/*
	 * Test constructor with filename
	 */
	@Test
	public void testMetricsConfiguration() {
		Configuration config = new Configuration(CONFIG_FILE);
		assertEquals(CONFIG_FILE,config.getFileName());
	}

	/*
	 * Test Constructor without filename
	 * Test setFileName()
	 */
	@Test
	public void testSetFileName() {
		Configuration config = new Configuration();
		config.setFileName(EMPTY_FILE);
		assertEquals(EMPTY_FILE,config.getFileName());
	}

	/*
	 * Test open with no file name
	 * Test open with missing file name
	 */
	@Test
	public void testOpenErrors() {
		// Configuration without file
		Configuration config = new Configuration();
		assertFalse(config.load());
		config.setFileName("bad-filename.txt");
		assertFalse(config.load());
	}

	/**
	 * test the default values (empty properties file)
	 */
	@Test
	public void testOpenDefault() {
		Configuration config = new Configuration(EMPTY_FILE);
		assertTrue(config.load());

		assertNull(config.getLinkFile());
		assertTrue(config.isLinkHeader());
		assertEquals(FieldDelimiter.TAB,config.getLinkDelimiter());
		assertEquals(1,config.getLinkRecordColumns().length);
		assertEquals(1,config.getLinkRecordColumns()[0]);
		assertEquals(2,config.getLinkClusterColumn());
		assertEquals(3,config.getLinkRuleColumn());
		assertEquals(3,config.getLinkRuleColumn());
		assertFalse(config.hasLinkFilter());
		assertEquals(0,config.getLinkFilterColumn());
		assertNull(config.getLinkFilterValue());
		
		assertNull(config.getReferenceFile());
		assertTrue(config.isReferenceHeader());
		assertEquals(FieldDelimiter.COMMA,config.getReferenceDelimiter());
		assertEquals(1,config.getReferenceRecordColumns().length);
		assertEquals(1,config.getReferenceRecordColumns()[0]);
		assertEquals(2,config.getReferenceClusterColumn());
		assertFalse(config.hasReferenceFilter());
		assertEquals(0,config.getReferenceFilterColumn());
		assertNull(config.getReferenceFilterValue());
		
		assertFalse(config.isOuterJoin());
		assertNull(config.getReportHistoryFilename());
		assertNull(config.getReportResultsFilename());
	}

	/**
	 * Test with all properties set to non-default values
	 */
	@Test
	public void testOpenSupplied() {
		Configuration config = new Configuration(TEST_FILE);
		assertTrue(config.load());

		assertEquals(config.getLinkFile(),"test.link");
		assertFalse(config.isLinkHeader());
		assertEquals(FieldDelimiter.SPACE,config.getLinkDelimiter());
		assertEquals(1,config.getLinkRecordColumns().length);
		assertEquals(3,config.getLinkRecordColumns()[0]);
		assertEquals(1,config.getLinkClusterColumn());
		assertEquals(2,config.getLinkRuleColumn());
		assertTrue(config.hasLinkFilter());
		assertEquals(4,config.getLinkFilterColumn());
		assertEquals("pair",config.getLinkFilterValue());
		
		assertEquals(config.getReferenceFile(),"truth.txt");
		assertFalse(config.isReferenceTruth());
		assertFalse(config.isReferenceHeader());
		assertEquals(FieldDelimiter.TAB,config.getReferenceDelimiter());
		assertEquals(2,config.getReferenceRecordColumns().length);
		assertEquals(2,config.getReferenceRecordColumns()[0]);
		assertEquals(3,config.getReferenceRecordColumns()[1]);
		assertEquals(1,config.getReferenceClusterColumn());
		assertTrue(config.hasReferenceFilter());
		assertEquals(5,config.getReferenceFilterColumn());
		assertEquals("match",config.getReferenceFilterValue());
		
		assertTrue(config.isOuterJoin());
	}
	
	@Test
	public void TestDiagnosticsMode() {
		Configuration config = new Configuration(EMPTY_FILE);
		assertTrue(config.load());
		assertFalse(config.isDiagnosticMode());
		
		config = new Configuration(TEST_FILE);
		assertTrue(config.load());
		assertTrue(config.isDiagnosticMode());
		assertTrue(config.isDiagnosticMode("SCAN"));
		assertTrue(config.isDiagnosticMode("SCAN","LIST"));
		assertTrue(config.isDiagnosticMode("LIST","SCAN","BAD"));
		assertFalse(config.isDiagnosticMode("BAD"));
		
		config = new Configuration(CONFIG_FILE);
		assertTrue(config.load());
		assertFalse(config.isDiagnosticMode());
		
	}
	
	@Test
	public void TestIsFileEncrypted() {
		Configuration config = new Configuration(EMPTY_FILE);
		EncryptedTruth et = config.encryptionFactory();
		assertNotNull(et);
		et.setFileName("Test.ert");
		assertTrue(et.isEncrypted());
		assertTrue(EncryptedTruth.isFileEncrypted("DataChallenge.ert"));
		assertFalse(EncryptedTruth.isFileEncrypted("DataChallenge.txt"));
	}

}
