package edu.ualr.er.metrics;

/*
 * Copyright 2020 James True
 * ERMetrics - Entity Resolution Metrics
 * edu.ualr.er.metrics.Assignment3PairTest
 *	
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *	
 *   http://www.apache.org/licenses/LICENSE-2.0
 *	
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

//import static org.junit.Assert.*;

import org.junit.Test;

public class Assignment3PairTest {

	@Test
	public void testMain() {
		System.out.println(">>>>>>>>>> Assignment3 Test >>>>>>>>>>>");
		ERMetrics.main(new String[] {"src/test/resources/Assignment3.pair.properties"});
		
		ERMetricsValues values = ERMetrics.values;
		assertNotNull(values);
		
		// Make sure we are reading the correct file
		assertEquals(32,values.getTotal().getCount());
		assertEquals(496,values.getTotal().getPairs());

		// Linked counts
		assertEquals(32,values.getLinked().getCount());
		assertEquals(16,values.getLinked().getClusters());
		assertEquals(29,values.getLinked().getPairs());

		// Reference counts
		assertEquals(32,values.getReference().getCount());
		assertEquals(14,values.getReference().getClusters());
		assertEquals(34,values.getReference().getPairs());

		// Associated counts
		assertEquals(32,values.getAssociated().getCount());
		assertEquals(19,values.getAssociated().getClusters());
		assertEquals(19,values.getAssociated().getPairs());

		// Assert the count calculations
		assertEquals(10,values.fp());
		assertEquals(452,values.tn());
		assertEquals(15,values.fn());
		
		// Assert the rate calculations
		assertEquals(0.021645,values.fpr(),0.00001);
		assertEquals(0.441176,values.fnr(),0.00001);
		assertEquals(0.949597,values.accuracy(),0.00001);
		assertEquals(0.655172,values.precision(),0.00001);
		assertEquals(0.558824,values.recall(),0.00001);
		assertEquals(0.603175,values.fMeasure(),0.00001);
		assertEquals(0.787717,values.twi(),0.00001);
	}

}
